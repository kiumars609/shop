import React from 'react'
import { useState } from 'react'
import Basket from './Basket'
import HeaderMenu from './HeaderMenu'
import HeaderMiddle from './HeaderMiddle'
// import HeaderTinyTop from './HeaderTinyTop'
import HeaderTopBar from './HeaderTopBar'

export default function Header({ cartItems }) {

    const [showModal, setShowModal] = useState(false);
    const handleCloseModal = () => setShowModal(false);
    const handleShowModal = () => setShowModal(true);
    return (
        <>
            <Basket cartItems={cartItems} showModal={showModal} handleShowModal={handleShowModal} handleCloseModal={handleCloseModal} />
            {/* <HeaderTinyTop /> */}
            <header className="header navbar-area">
                <HeaderTopBar />
                <HeaderMiddle cartItems={cartItems} handleShowModal={handleShowModal} handleCloseModal={handleCloseModal} />
                <HeaderMenu />
            </header>
        </>
    )
}
