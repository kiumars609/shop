import React from 'react'
import { Link } from 'react-router-dom'
import Search from '../../../Components/Search'
import './style.css'

export default function HeaderMiddle({ cartItems, handleShowModal }) {
    return (
        <>
            <div className="header-middle">
                <div className="container">
                    <div className="row align-items-center">
                        <div className="col-lg-3 col-md-3 col-7">
                            <Link to='/' className="navbar-brand">
                                <img src="/assets/main/images/logo/logo2.png" alt="Logo" />
                            </Link>
                        </div>
                        <div className="col-lg-5 col-md-7 d-xs-none">
                            <div className="main-menu-search">
                                <div className="navbar-search search-style-5">
                                    <div className="search-input">
                                        <Search api={'/products/search/searchAPI.php'}>
                                            <input type="text" placeholder="Search" />
                                        </Search>
                                    </div>
                                    <div className="search-btn">
                                        <button><i className="lni lni-search-alt"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-2 col-5">
                            <div className="middle-right-area">
                                <div className="nav-hotline">
                                    <i className="lni lni-phone"></i>
                                    <h3>Hotline:
                                        <span>(+98) 915 504 2678</span>
                                    </h3>
                                </div>
                                <div className="navbar-cart">
                                    <div className="wishlist">
                                        <a href="{void(0)}">
                                            <i className="lni lni-heart"></i>
                                            <span className="total-items">0</span>
                                        </a>
                                    </div>
                                    <div className="cart-items">



                                        <span className="main-btn" role="button" onClick={handleShowModal}>
                                            <i className="lni lni-cart"></i>
                                            <span className="total-items">{cartItems.length === 0 ? 0 : cartItems.length}</span>
                                        </span>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
