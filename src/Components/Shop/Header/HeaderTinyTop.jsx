import React from 'react'

export default function HeaderTinyTop() {
    return (
        <>
            {/* <!--[if lte IE 9]> */}
            <p className="browserupgrade">
                You are using an <strong>outdated</strong> browser. Please
                <a href="https://browsehappy.com/">upgrade your browser</a> to improve
                your experience and security.
            </p>
            {/* <![endif]--> */}
        </>
    )
}
