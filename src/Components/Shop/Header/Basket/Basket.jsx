import React from 'react'
import { Link } from 'react-router-dom'
import './style.css'
import Modal from 'react-bootstrap/Modal';

export default function Basket({ cartItems, showModal, handleCloseModal, handleShowModal }) {

    const totalPrice = cartItems.reduce((price, item) => price + item.quantity * item.price, 0)
    function commaSeparators(value) {
        return value.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
    }
    return (
        <>
            <Modal className='modal-right modal-card' size="sm" show={showModal} onHide={handleCloseModal}>
                <Modal.Header closeButton>
                    {/* <Modal.Title>Modal heading</Modal.Title> */}
                </Modal.Header>
                <Modal.Body>
                    <div className="shopping-item">
                        <ul className="shopping-list">
                            {cartItems.length === 0 && <div className='col-md-12 p-3'>No Items</div>}
                            {cartItems.map((item, index) => {
                                return (
                                    <li key={index}>
                                        <a href="true" className="remove" title="Remove this item"><i
                                            className="lni lni-close"></i></a>
                                        <div className="cart-img-head">
                                            <Link className="cart-img" to={`/product/${item.category}/${item.id}`}><img
                                                src={`/assets/main/images/products/${item.category}/${item.img1}`} alt="#" /></Link>
                                        </div>
                                        <div className="content">
                                            <h4><Link to={`/product/${item.category}/${item.id}`}>{item.titleSmall}</Link></h4>
                                            <p className="quantity">{item.quantity}x - <span className="amount">€{commaSeparators(item.price)}</span></p>
                                        </div>
                                    </li>
                                )
                            })}
                        </ul>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <div className="bottom col-lg-12 col-md-12 col-12">
                        <div className="total">
                            <span>Total</span>
                            <span className="total-amount">€{commaSeparators(totalPrice)}</span>
                        </div>
                    </div>
                    <Link to='/cart' className="btn btn-primary col-lg-12 col-md-12 col-12" onClick={handleCloseModal}>View Card</Link>
                    <Link to='/checkout' className="btn btn-success-new col-lg-12 col-md-12 col-12" onClick={handleCloseModal}>Checkout</Link>
                </Modal.Footer>
            </Modal>
        </>
    )
}
