import React from 'react'
import { Link } from 'react-router-dom'
import ActionLogOut from '../../../Context/ActionLogOut'
import { User_loggedin } from '../../../Context/User_loggedin'
// import { useAuthState } from '../../../Context/auth-context'

export default function HeaderTopBar() {
    const userInfo = User_loggedin()
    const exit = ActionLogOut()
    return (
        <>
            <div className="topbar">
                <div className="container">
                    <div className="row align-items-center">
                        <div className="col-lg-4 col-md-4 col-12">
                            <div className="top-left">
                                <ul className="menu-top-link">
                                    <li>
                                        <div className="select-position">
                                            <select id="select4">
                                                <option value="0" defaultValue="">€ EURO</option>
                                                <option value="1">$ USD</option>
                                            </select>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="select-position">
                                            <select id="select5">
                                                <option value="0" defaultValue="">English</option>
                                                <option value="1">Germany</option>
                                            </select>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-4 col-12">
                            <div className="top-middle">
                                <ul className="useful-links">
                                    {/* <li><Link to='/about-us'>About Us</Link></li>
                                    <li><Link to='/contact-us'>Contact Us</Link></li> */}
                                </ul>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-4 col-12">
                            <div className="top-end">
                                {userInfo ?
                                    <>


                                        <nav className="navbar">
                                            <li className="nav-item dropdown">
                                                <span className="dropdown-toggle" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                    <div className="user">
                                                        <i className="lni lni-user"></i>
                                                        Hello {userInfo.fname}
                                                        <i className="lni lni-chevron-down arrow-down"></i>
                                                    </div>
                                                </span>
                                                <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                                                    <Link to={'/profile'} className='lnks col-12'>
                                                        <li className='position-relative main-user-detail dropdown-item' role='button'>
                                                            <div className="row p-0 mx-auto">
                                                                <div className="col-3 py-2 px-0">
                                                                    <img src={`/assets/main/images/users/avatar/${userInfo.avatar}`} className='avatar' alt='avatar' />
                                                                </div>
                                                                <div className="col-8 p-0">
                                                                    <li><a className="dropdown-item full-name" href="true">{userInfo.fname + ' ' + userInfo.lname}</a></li>
                                                                </div>

                                                            </div>
                                                            <i className="lni lni-chevron-right right-arrow"></i>
                                                        </li>
                                                    </Link>
                                                    <li><span className="dropdown-item links-li" role='button' onClick={exit}>
                                                        <span>LogOut</span>
                                                    </span></li>
                                                </ul>
                                            </li>
                                        </nav>

                                    </>
                                    :
                                    <ul className="user-login">
                                        <li>
                                            <Link to={'/login'}>Sign In</Link>
                                        </li>
                                        <li>
                                            <Link to={'/register'}>Register</Link>
                                        </li>
                                    </ul>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
