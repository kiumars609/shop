import React from 'react'
import { Link } from 'react-router-dom'

export default function HeaderMenu() {
    return (
        <>
            <div className="container">
                <div className="row align-items-center">
                    <div className="col-lg-8 col-md-6 col-12">
                        <div className="nav-inner">

                            <div className="mega-category-menu">
                                <span className="cat-button"><i className="lni lni-menu"></i>All Categories</span>
                                <ul className="sub-category">
                                    <li><a href="./">Electronics <i className="lni lni-chevron-right"></i></a>
                                        <ul className="inner-sub-category">
                                            <li><Link to='/shop/laptop'>Laptop</Link></li>
                                            <li><Link to='/shop/laptops-accessories'>Laptops Accessories</Link></li>
                                            <li><Link to='/shop/mobile'>Mobile</Link></li>
                                            <li><Link to='/shop/mobiles-accessories'>Mobiles Accessories</Link></li>
                                            <li><Link to='/shop/smart-watches'>Smart Watches</Link></li>
                                            <li><Link to='/shop/console'>Console</Link></li>
                                            <li><Link to='/shop/consoles-accessories'>Consoles Accessories</Link></li>
                                            <li><Link to='/shop/headphones'>Headphones</Link></li>
                                            <li><Link to='/shop/microphones'>Microphones</Link></li>
                                            <li><Link to='/shop/televisions'>Televisions</Link></li>
                                            <li><Link to='/shop/batteries'>Batteries</Link></li>
                                            <li><Link to='/shop/digital-cameras'>Digital Cameras</Link></li>
                                            <li><Link to='/shop/cables-adapters'>Cables & Adapters</Link></li>
                                        </ul>
                                    </li>
                                    <li><a href="./">Clothes <i className="lni lni-chevron-right"></i></a>
                                        <ul className="inner-sub-category">
                                            <li><Link to='/shop/woman'>Women</Link></li>
                                            <li><Link to='/shop/men'>Men</Link></li>
                                            <li><Link to='/shop/kids'>Kids</Link></li>
                                        </ul>
                                    </li>
                                    <li><Link to='/shop/beauty-health'>Beauty and Health</Link></li>
                                    <li><Link to='/shop/home-kitchen'>Home & Kitchen</Link></li>
                                    <li><Link to='/shop/coffee-espresso-machines'>Coffee & Espresso Machines</Link></li>
                                    <li><Link to='/shop/home-decore'>Home Decore</Link></li>
                                    <li><Link to='/shop/drugstore'>Drugstore</Link></li>
                                    <li><Link to='/shop/babycare'>Babycare</Link></li>
                                    <li><Link to='/shop/books'>Books</Link></li>
                                </ul>
                            </div>


                            <nav className="navbar navbar-expand-lg">
                                <button className="navbar-toggler mobile-menu-btn" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                                    aria-expanded="false" aria-label="Toggle navigation">
                                    <span className="toggler-icon"></span>
                                    <span className="toggler-icon"></span>
                                    <span className="toggler-icon"></span>
                                </button>
                                <div className="collapse navbar-collapse sub-menu-bar" id="navbarSupportedContent">
                                    <ul id="nav" className="navbar-nav ms-auto">
                                        <li className="nav-item">
                                            <Link to='/' aria-label="Toggle navigation">Home</Link>
                                        </li>
                                        <li className="nav-item">
                                            <Link to='/cart' aria-label="Toggle navigation">Cart</Link>
                                        </li>
                                        <li className="nav-item">
                                            <Link to='/blog' aria-label="Toggle navigation">Blog</Link>
                                        </li>
                                        <li className="nav-item">
                                            <Link to='/about-us' aria-label="Toggle navigation">About Us</Link>
                                        </li>
                                        <li className="nav-item">
                                            <Link to='/contact-us' aria-label="Toggle navigation">Contact Us</Link>
                                        </li>
                                        {/* <li className="nav-item">
                                            <a className="dd-menu collapsed" href="{void(0)}" data-bs-toggle="collapse"
                                                data-bs-target="#submenu-1-2" aria-controls="navbarSupportedContent"
                                                aria-expanded="false" aria-label="Toggle navigation">Pages</a>
                                            <ul className="sub-menu collapse" id="submenu-1-2">
                                                <li className="nav-item"><a href="about-us.html">About Us</a></li>
                                                <li className="nav-item"><a href="faq.html">Faq</a></li>
                                                <li className="nav-item"><a href="login.html">Login</a></li>
                                                <li className="nav-item"><a href="register.html">Register</a></li>
                                                <li className="nav-item"><a href="mail-success.html">Mail Success</a></li>
                                                <li className="nav-item"><a href="404.html">404 Error</a></li>
                                            </ul>
                                        </li> */}
                                    </ul>
                                </div>
                            </nav>

                        </div>
                    </div>
                    <div className="col-lg-4 col-md-6 col-12">

                        <div className="nav-social">
                            <h5 className="title">Follow Us:</h5>
                            <ul>
                                <li>
                                    <a href="{void(0)}"><i className="lni lni-instagram"></i></a>
                                </li>
                                <li>
                                    <a href="{void(0)}"><i className="lni lni-telegram"></i></a>
                                </li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </>
    )
}
