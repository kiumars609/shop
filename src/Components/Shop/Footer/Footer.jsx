import { Button } from 'bootstrap'
import React from 'react'
import { Link } from 'react-router-dom'
import './style.css'

export default function Footer() {
    return (
        <>
            <footer className="footer">
                <div className="footer-top">
                    <div className="container">
                        <div className="inner-content">
                            <div className="row">
                                <div className="col-lg-3 col-md-4 col-12">
                                    <div className="footer-logo">
                                        <Link to={'/'}>
                                            <img src="/assets/main/images/logo/logo3.png" alt="Logo" />
                                        </Link>
                                    </div>
                                </div>
                                <div className="col-lg-9 col-md-8 col-12">
                                    <div className="footer-newsletter">
                                        <h4 className="title">
                                            Subscribe to our Newsletter
                                            <span>Get all the latest information, Sales and Offers.</span>
                                        </h4>
                                        <div className="newsletter-form-head">
                                            <form className="newsletter-form">
                                                <input name="EMAIL" placeholder="Email address here..." type="email" />
                                                <div className="button">
                                                    <span className="btn" data-bs-toggle="modal" data-bs-target="#comingSoonModal">Subscribe<span className="dir-part"></span></span>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="footer-middle">
                    <div className="container">
                        <div className="bottom-inner">
                            <div className="row">
                                <div className="col-lg-3 col-md-6 col-12">
                                    <div className="single-footer f-contact">
                                        <h3>Get In Touch With Us</h3>
                                        <p className="phone">Phone: +98 (915) 504 26 78</p>
                                        <ul>
                                            <li><span>Monday-Friday: </span> 9.00 am - 8.00 pm</li>
                                            <li><span>Saturday: </span> 10.00 am - 6.00 pm</li>
                                        </ul>
                                        <p className="mail">
                                            <a href="mailto:support@shopgrids.com">support@mr-kiumars.ir</a>
                                        </p>
                                    </div>
                                </div>
                                <div className="col-lg-3 col-md-6 col-12">
                                    <div className="single-footer our-app">
                                        <h3>Our Mobile App</h3>
                                        <ul className="app-btn">
                                            <li>
                                                <a href="true" data-bs-toggle="modal" data-bs-target="#comingSoonModal">
                                                    <i className="lni lni-apple"></i>
                                                    <span className="small-title">Download on the</span>
                                                    <span className="big-title">App Store</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="true" data-bs-toggle="modal" data-bs-target="#comingSoonModal">
                                                    <i className="lni lni-play-store"></i>
                                                    <span className="small-title">Download on the</span>
                                                    <span className="big-title">Google Play</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="col-lg-3 col-md-6 col-12">
                                    <div className="single-footer f-link">
                                        <h3>Information</h3>
                                        <ul>
                                            <li><Link to={'/about-us'}>About Us</Link></li>
                                            <li><Link to={'/contact-us'}>Contact Us</Link></li>
                                            <li><Link to={'/blog'}>Blog</Link></li>
                                            <li><Link to={'/cart'}>Cart</Link></li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="col-lg-3 col-md-6 col-12">
                                    <div className="single-footer f-link">
                                        <h3>Shop Departments</h3>
                                        <ul>
                                            <li><Link to={'/shop/laptop'}>Laptop</Link></li>
                                            <li><Link to={'/shop/mobile'}>Mobile</Link></li>
                                            <li><Link to={'/shop/console'}>Console</Link></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="footer-bottom">
                    <div className="container">
                        <div className="inner-content">
                            <div className="row align-items-center">
                                <div className="col-lg-4 col-12">
                                    <div className="payment-gateway">
                                        <span>We Accept:</span>
                                        <img src="/assets/main/images/footer/credit-cards-footer.png" alt="#" />
                                    </div>
                                </div>
                                <div className="col-lg-4 col-12">
                                    <div className="copyright">
                                        <p>Designed and Developed by<a href="https://mr-kiumars.ir/" target={'_blank'} rel="noreferrer nopenner">Kiumars Sezavar</a></p>
                                    </div>
                                </div>
                                <div className="col-lg-4 col-12">
                                    <ul className="socila">
                                        <li>
                                            <span>Follow Us On:</span>
                                        </li>
                                        <li><a href="https://www.instagram.com/about.kiumars/" target={'_blank'} rel="noreferrer nopenner"><i className="lni lni-instagram"></i></a></li>
                                        <li><a href="https://t.me/Kiumars_Gamer" target={'_blank'} rel="noreferrer nopenner"><i className="lni lni-telegram"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <a href="#" className="scroll-top">
                <i className="lni lni-chevron-up"></i>
            </a>
            {/* <!-- Modal Comming Soon Applications --> */}
            <div className="modal fade" id="comingSoonModal" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content footer-modal-body">
                        <div className="modal-body text-center">
                            <img src="/assets/main/images/icons/comingSoon.png" alt="coming-soon" />
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
