import React, { useEffect } from 'react'
import { AuthProvider } from '../../../Context/auth-context';
import Footer from '../Footer'
import Header from '../Header'
import PreLoader from '../PreLoader'

export default function Layout({ children, pgTitle = 'DigiWare', cartItems }) {

    useEffect(() => {
        document.title = 'DigiWare | ' + pgTitle;
    }, [pgTitle]);

    return (
        <>
            <AuthProvider>
                <PreLoader />
                <Header cartItems={cartItems} />
                {children}
                <Footer />
            </AuthProvider>
        </>
    )
}
