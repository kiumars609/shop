import React from 'react'
import { Link } from 'react-router-dom'
import { useCommaSeparators } from '../../../../CustomHooks/useCommaSeparators'

export default function TrendingProductSingle({ data }) {
    return (
        <>
            <div className="col-lg-3 col-md-6 col-12">
                <div className="single-product">
                    <div className="product-image">
                        <img src={`./assets/main/images/products/${data.category}/${data.img1}`} alt="#" />
                        <div className="button">
                            <Link to={`/product/${data.category}/${data.id}`} className="btn"><i className="lni lni-cart"></i> More Info</Link>
                        </div>
                    </div>
                    <div className="product-info">
                        <span className="category">{data.category}</span>
                        <h4 className="title">
                            <a href={data.link}>{data.titleSmall}</a>
                        </h4>
                        <ul className="review">
                            <li><i className="lni lni-star-filled"></i></li>
                            <li><i className="lni lni-star-filled"></i></li>
                            <li><i className="lni lni-star-filled"></i></li>
                            <li><i className="lni lni-star-filled"></i></li>
                            <li><i className="lni lni-star"></i></li>
                            <li><span>{data.review} Review(s)</span></li>
                        </ul>
                        <div className="price">
                            <span>€{useCommaSeparators(data.price)}</span>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
