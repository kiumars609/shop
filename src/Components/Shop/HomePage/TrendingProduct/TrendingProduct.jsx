import React from 'react'
import TrendingProductSingle from './TrendingProductSingle'
import './style.css'

export default function TrendingProduct({ title, data, describe='' }) {

    const listsMap = data.map(items => {
        return (
            <TrendingProductSingle key={items.id} data={items} />
        )
    })
    return (
        <>
            <section className="trending-product section">
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <div className="section-title">
                                <h2>{title}</h2>
                                <p>{describe}</p>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        {listsMap}
                    </div>
                </div>
            </section>
        </>
    )
}