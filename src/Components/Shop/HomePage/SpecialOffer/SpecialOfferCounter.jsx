import React from 'react'

export default function SpecialOfferCounter({ data }) {
    return (
        <>
            <div className="col-lg-4 col-md-12 col-12">
                <div className="offer-content">
                    <div className="image">
                        <img src={data.image} alt="#" />
                        <span className="sale-tag">{data.sale}%</span>
                    </div>
                    <div className="text">
                        <h2><a href={data.link}>{data.title}</a></h2>
                        <ul className="review">
                            <li><i className="lni lni-star-filled"></i></li>
                            <li><i className="lni lni-star-filled"></i></li>
                            <li><i className="lni lni-star-filled"></i></li>
                            <li><i className="lni lni-star-filled"></i></li>
                            <li><i className="lni lni-star-filled"></i></li>
                            <li><span>{data.review} Review(s)</span></li>
                        </ul>
                        <div className="price">
                            <span>${data.price}</span>
                            <span className="discount-price">${data.oldPrice}</span>
                        </div>
                        <p>{data.describe}</p>
                    </div>
                    <div className="box-head">
                        <div className="box">
                            <h1 id="days">174</h1>
                            <h2 id="daystxt">Days</h2>
                        </div>
                        <div className="box">
                            <h1 id="hours">12</h1>
                            <h2 id="hourstxt">Hours</h2>
                        </div>
                        <div className="box">
                            <h1 id="minutes">54</h1>
                            <h2 id="minutestxt">Minutes</h2>
                        </div>
                        <div className="box">
                            <h1 id="seconds">54</h1>
                            <h2 id="secondstxt">Secondes</h2>
                        </div>
                    </div>
                    <div style={{ background: 'rgb(204, 24, 24)' }} className="alert">
                        <h1 style={{ padding: '50px 80px', color: '#fff' }}>We are sorry, Event ended ! </h1>
                    </div>
                </div>
            </div>
        </>
    )
}
