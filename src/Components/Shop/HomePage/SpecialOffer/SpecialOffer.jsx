import React from 'react'
import SpecialOfferBanner from './SpecialOfferBanner'
import SpecialOfferCounter from './SpecialOfferCounter'
import SpecialOfferSingle from './SpecialOfferSingle'

export default function SpecialOffer() {

    const banner = { title: 'Samsung Notebook 9', image: './assets/main/images/banner/banner-3-bg.jpg', describe: 'Lorem ipsum dolor sit amet,', describe2: 'eiusmod tempor incididunt ut labore.', price: '499.00', link: 'product-grids.html' }

    const counter = { title: 'Bluetooth Headphone', image: './assets/main/images/offer/offer-image.jpg', sale: '-50', review: '5.0', price: '200.00', oldPrice: '400.00', describe: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry incididunt ut eiusmod tempor labores.', link: 'product-grids.html' }

    const lists = [
        { title: 'Xiaomi Mi Band 5', image: './assets/main/images/products/product-1.jpg', link: 'product-details.html', category: 'Watches', review: '4.0', price: '199.00' },
        { title: 'Big Power Sound Speaker', image: './assets/main/images/products/product-2.jpg', link: 'product-details.html', category: 'Speaker', review: '5.0', price: '275.00' },
        { title: 'WiFi Security Camera', image: './assets/main/images/products/product-3.jpg', link: 'product-details.html', category: 'Camera', review: '3.0', price: '399.00' },
    ]

    const listsMap = lists.map((items, index) => {
        return (
            <SpecialOfferSingle key={index} data={items} />
        )
    })

    return (
        <>
            <section className="special-offer section">
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <div className="section-title">
                                <h2>Special Offer</h2>
                                <p>There are many variations of passages of Lorem Ipsum available, but the majority have
                                    suffered alteration in some form.</p>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-8 col-md-12 col-12">
                            <div className="row">
                                {listsMap}
                            </div>
                            <SpecialOfferBanner data={banner} />
                        </div>
                        <SpecialOfferCounter data={counter} />
                    </div>
                </div>
            </section>
        </>
    )
}
