import React from 'react'

export default function SpecialOfferBanner({ data }) {
    return (
        <>
            <div className="single-banner right"
                style={{ backgroundImage: "url(" + `${data.image}` + ")", marginTop: '30px' }}>
                <div className="content">
                    <h2>{data.title} </h2>
                    <p>{data.describe} <br />{data.describe2}</p>
                    <div className="price">
                        <span>${data.price}</span>
                    </div>
                    <div className="button">
                        <a href={data.link} className="btn">Shop Now</a>
                    </div>
                </div>
            </div>
        </>
    )
}
