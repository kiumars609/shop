import React from 'react'

export default function SpecialOfferSingle({ data }) {
    return (
        <>
            <div className="col-lg-4 col-md-4 col-12">
                <div className="single-product">
                    <div className="product-image">
                        <img src={data.image} alt="#" />
                        <div className="button">
                            <a href={data.link} className="btn"><i className="lni lni-cart"></i> Add to
                                Cart</a>
                        </div>
                    </div>
                    <div className="product-info">
                        <span className="category">{data.category}</span>
                        <h4 className="title">
                            <a href={data.link}>{data.title}</a>
                        </h4>
                        <ul className="review">
                            <li><i className="lni lni-star-filled"></i></li>
                            <li><i className="lni lni-star-filled"></i></li>
                            <li><i className="lni lni-star-filled"></i></li>
                            <li><i className="lni lni-star-filled"></i></li>
                            <li><i className="lni lni-star-filled"></i></li>
                            <li><span>{data.review} Review(s)</span></li>
                        </ul>
                        <div className="price">
                            <span>${data.price}</span>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

