import React from 'react'
import TopProductsSingle from './TopProductsSingle'
import { lists } from './lists'

export default function TopProducts() {

    const item = lists();
    return (
        <>
            <section className="home-product-list section">
                <div className="container">
                    <div className="row">
                        <TopProductsSingle data={item.bestSellers} />
                        <TopProductsSingle data={item.newArrivals} />
                        <TopProductsSingle data={item.topRated} />
                    </div>
                </div>
            </section>
        </>
    )
}
