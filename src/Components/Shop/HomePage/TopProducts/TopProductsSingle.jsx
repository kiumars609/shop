import React from 'react'

export default function TopProductsSingle({ data }) {

    const title = data[0].title;
    const items = data[0].items;
    const listsMap = items.map((item, index) => {
        return (
            <div key={index} className="single-list">
                <div className="list-image">
                    <a href={item.link}><img src={item.image} alt="#" /></a>
                </div>
                <div className="list-info">
                    <h3>
                        <a href="product-grids.html">{item.title}</a>
                    </h3>
                    <span>${item.price}</span>
                </div>
            </div>
        )
    })

    return (
        <>
            <div className="col-lg-4 col-md-4 col-12 custom-responsive-margin">
                <h4 className="list-title">{title}</h4>
                {listsMap}
            </div>
        </>
    )
}
