export function lists() {
    const bestSellers = [
        {
            title: 'Best Sellers',
            items: [
                { title: 'GoPro Hero4 Silver', image: './assets/main/images/home-product-list/01.jpg', price: '1150.00', link: 'product-grids.html' },
                { title: 'Puro Sound Labs BT2200', image: './assets/main/images/home-product-list/02.jpg', price: '95.00', link: 'product-grids.html' },
                { title: 'HP OfficeJet Pro 8710', image: './assets/main/images/home-product-list/03.jpg', price: '120.00', link: 'product-grids.html' },
            ]
        }
    ]

    const newArrivals = [
        {
            title: 'New Arrivals',
            items: [
                { title: 'iPhone X 256 GB Space Gray', image: './assets/main/images/home-product-list/04.jpg', price: '1150.00', link: 'product-grids.html' },
                { title: 'Canon EOS M50 Mirrorless Camera', image: './assets/main/images/home-product-list/05.jpg', price: '95.00', link: 'product-grids.html' },
                { title: 'Microsoft Xbox One S', image: './assets/main/images/home-product-list/06.jpg', price: '298.00', link: 'product-grids.html' },
            ]
        }
    ]

    const topRated = [
        {
            title: 'Top Rated',
            items: [
                { title: 'Samsung Gear 360 VR Camera', image: './assets/main/images/home-product-list/07.jpg', price: '68.00', link: 'product-grids.html' },
                { title: 'Samsung Galaxy S9+ 64 GB', image: './assets/main/images/home-product-list/08.jpg', price: '840.00', link: 'product-grids.html' },
                { title: 'Zeus Bluetooth Headphones', image: './assets/main/images/home-product-list/09.jpg', price: '28.00', link: 'product-grids.html' },
            ]
        }
    ]

    return { bestSellers, newArrivals, topRated }
}
