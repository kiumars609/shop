import React from 'react'
import BannerSingle from './BannerSingle'

export default function Banner() {

    const lists = [
        { title: 'Smart Watch 2.0', image: './assets/main/images/banner/banner-1-bg.jpg', describe: 'Space Gray Aluminum Case with', describe2: 'Black/Volt Real Sport Band', link: 'product-grids.html' },
        { title: 'Smart Headphone', image: './assets/main/images/banner/banner-2-bg.jpg', describe: 'Lorem ipsum dolor sit amet,', describe2: 'eiusmod tempor incididunt ut labore.', link: 'product-grids.html' },
    ]

    const listsMap = lists.map((items, index) => {
        return (
            <BannerSingle key={index} data={items} />
        )
    })

    return (
        <>
            <section className="banner section">
                <div className="container">
                    <div className="row">
                        {listsMap}
                    </div>
                </div>
            </section>
        </>
    )
}
