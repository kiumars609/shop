import React from 'react'

export default function BannerSingle({ data }) {
    return (
        <>
            <div className="col-lg-6 col-md-6 col-12">
                <div className="single-banner" style={{ backgroundImage: "url(" + `${data.image}` + ")" }}>
                    <div className="content">
                        <h2>{data.title}</h2>
                        <p>{data.describe} <br />{data.describe2} </p>
                        <div className="button">
                            <a href={data.link} className="btn">View Details</a>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
