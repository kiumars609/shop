import React from 'react'
import HeroAreaBanner from './HeroAreaBanner'
import HeroAreaSlider from './HeroAreaSlider'

export default function HeroArea() {
    return (
        <>
            <section className="hero-area">
                <div className="container">
                    <div className="row">
                        <HeroAreaSlider />
                        <HeroAreaBanner />
                    </div>
                </div>
            </section>
        </>
    )
}
