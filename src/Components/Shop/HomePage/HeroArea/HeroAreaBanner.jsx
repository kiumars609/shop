import React from 'react'

export default function HeroAreaBanner() {
    return (
        <>
            <div className="col-lg-4 col-12">
                <div className="row">
                    <div className="col-lg-12 col-md-6 col-12 md-custom-padding">
                        <div className="hero-small-banner"
                            style={{ backgroundImage: `url(./assets/main/images/hero/slider-bnr.jpg)` }}>
                            <div className="content">
                                <h2>
                                    <span>New line required</span>
                                    iPhone 12 Pro Max
                                </h2>
                                <h3>$259.99</h3>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-12 col-md-6 col-12">
                        <div className="hero-small-banner style2">
                            <div className="content">
                                <h2>Weekly Sale!</h2>
                                <p>Saving up to 50% off all online store items this week.</p>
                                <div className="button">
                                    <a className="btn" href="product-grids.html">Shop Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
