export function lists() {
    const categoryTV = [
        { title: 'TV & Audios', image: './assets/main/images/featured-categories/fetured-item-1.png', size: null, linkTitle1: 'Samsung', linkTitle2: 'LG', linkTitle3: 'Philips', linkTitle4: 'TCL', link1: '#', link2: '#', link3: '#', link4: '#', linkAll: '#' }
    ]
    const categoryDesktop = [
        { title: 'Desktop & Laptop', image: './assets/main/images/featured-categories/fetured-item-2.png', size: null, linkTitle1: 'Green', linkTitle2: 'Asus', linkTitle3: 'HP', linkTitle4: 'MSI', link1: '#', link2: '#', link3: '#', link4: '#', linkAll: '#' }
    ]
    const categoryCCTVCamera = [
        { title: 'Cctv Camera', image: './assets/main/images/featured-categories/fetured-item-3.png', size: null, linkTitle1: 'Dahua', linkTitle2: 'Zicom', linkTitle3: 'D-LINK', linkTitle4: 'Panasonic', link1: '#', link2: '#', link3: '#', link4: '#', linkAll: '#' }
    ]
    const categoryDSLRCamera = [
        { title: 'Dslr Camera', image: './assets/main/images/featured-categories/fetured-item-4.png', size: null, linkTitle1: 'Canon', linkTitle2: 'Fujifilm', linkTitle3: 'Sony', linkTitle4: 'Nikon', link1: '#', link2: '#', link3: '#', link4: '#', linkAll: '#' }
    ]
    const categoryPhone = [
        { title: 'Smart Phones', image: './assets/main/images/featured-categories/fetured-item-5.webp', size: '135', linkTitle1: 'Apple', linkTitle2: 'Samsung', linkTitle3: 'Xiaomi', linkTitle4: 'Huawei', link1: '#', link2: '#', link3: '#', link4: '#', linkAll: '#' }
    ]
    const categoryConsole = [
        { title: 'Game Console', image: './assets/main/images/featured-categories/fetured-item-6.webp', size: null, linkTitle1: 'PlayStation', linkTitle2: 'Microsoft', linkTitle3: 'Nintendo', linkTitle4: '', link1: '#', link2: '#', link3: '#', link4: '#', linkAll: '' }
    ]

    return { categoryTV, categoryDesktop, categoryCCTVCamera, categoryDSLRCamera, categoryPhone, categoryConsole }
}
