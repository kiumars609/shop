import React from 'react'

export default function FeaturedCategoriesSingle({ data }) {
    const item = data[0];

    return (
        <>
            <div className="col-lg-4 col-md-6 col-12">
                <div className="single-category">
                    <h3 className="heading">{item.title}</h3>
                    <ul>
                        <li><a href={item.link1}>{item.linkTitle1}</a></li>
                        <li><a href={item.link2}>{item.linkTitle2}</a></li>
                        <li><a href={item.link3}>{item.linkTitle3}</a></li>
                        <li><a href={item.link4}>{item.linkTitle4}</a></li>
                        <li><a href={item.linkAll}>{'' !== item.linkAll ? 'View All' : ''}</a></li>
                    </ul>
                    <div className="images">
                        {null !== item.size ? <img src={item.image} width={item.size} alt="#" /> : <img src={item.image} width="201" alt="#" />}
                    </div>
                </div>
            </div>
        </>
    )
}
