import React from 'react'
import FeaturedCategoriesSingle from './FeaturedCategoriesSingle'
import { lists } from './lists'

export default function FeaturedCategories() {
    const category = lists()
    return (
        <>
            <section className="featured-categories section">
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <div className="section-title">
                                <h2>Featured Categories</h2>
                                <p>There are many variations of passages of Lorem Ipsum available, but the majority have
                                    suffered alteration in some form.</p>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <FeaturedCategoriesSingle data={category.categoryDesktop} />
                        <FeaturedCategoriesSingle data={category.categoryPhone} />
                        <FeaturedCategoriesSingle data={category.categoryConsole} />
                        <FeaturedCategoriesSingle data={category.categoryTV} />
                        <FeaturedCategoriesSingle data={category.categoryCCTVCamera} />
                        <FeaturedCategoriesSingle data={category.categoryDSLRCamera} />
                    </div>
                </div>
            </section>
        </>
    )
}
