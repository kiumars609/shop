import React from 'react'
import { Link } from 'react-router-dom';

export default function BlogSingle({ data, newCategory }) {
    return (
        <>
            <div className="col-lg-4 col-md-6 col-12">
                <div className="single-blog">
                    <div className="blog-img">
                        <Link to={`/blog/${data.id}`}>
                            <img src={`/assets/main/images/blog/${data.img}`} alt={data.title} />
                        </Link>
                    </div>
                    <div className="blog-content">
                        {/* <a className="category" href="{void(0)}">{data.category}</a> */}
                        {newCategory}
                        <h4>
                            <Link to={`/blog/${data.id}`}>{data.title}</Link>
                        </h4>
                        <p>{data.describe}</p>
                        <div className="button">
                            <Link to={`/blog/${data.id}`} className="btn">Read More</Link>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
