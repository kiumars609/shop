import React from 'react'
import { useState, useEffect } from 'react';
import { get } from '../../../../services';
import Loading from '../../../Loading';
import BlogSingle from './BlogSingle'

export default function Blog() {

    const [data, setData] = useState([])
    const [loader, setLoader] = useState(true)

    useEffect(() => {
        get('/blog/blogAPI.php')
            .then(response => {
                setData(response)
                setLoader(false)
            })
    }, []);

    const mapData = data && data.slice(0, 3).map((item, index) => {
        const category = item.category.split(',')
        const newCategory = category.map((i, index) => {
            return (
                <a key={index} className="category blog-categories" href={`/blog/${i}`}>{i + '\u00A0'}</a>
            )
        })
        return (
            <BlogSingle key={index} data={item} newCategory={newCategory} />
        )
    })
    return (
        <>
            <section className="blog-section section">
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <div className="section-title">
                                <h2>Our Latest News</h2>
                                <p>There are many variations of passages of Lorem
                                    Ipsum available, but the majority have suffered alteration in some form.</p>
                            </div>
                        </div>
                    </div>
                    {loader ?
                        <Loading /> :
                        <div className="row">
                            {mapData}
                        </div>
                    }
                </div>
            </section>
        </>
    )
}
