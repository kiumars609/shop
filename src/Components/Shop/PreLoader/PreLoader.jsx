import React from 'react'

export default function PreLoader() {
    return (
        <>
            <div className="preloader" style={{ opacity: '0', display: 'none' }}>
                <div className="preloader-inner">
                    <div className="preloader-icon">
                        <span></span>
                        <span></span>
                    </div>
                </div>
            </div>
        </>
    )
}
