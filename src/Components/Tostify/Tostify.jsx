import React from 'react'
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export default function Tostify({ position = "bottom-right", width }) {
    return (
        <>
            <ToastContainer
                style={{ width: width }}
                position={position}
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme='colored'
                width="200px"
            />

            {/* 
                //////Document//////
                    -Add Below Codes in your Project:
                        toast['success']('Blog Inserted Successfully !!')

                    -And This One to Your Return:
                        <Tostify />
            */}

        </>
    )
}
