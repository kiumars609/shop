import React from 'react'
import { Navigate } from 'react-router-dom'
import { useAuthState } from '../../../Context/auth-context'
import Footer from './Footer/Footer'
import SideBar from './SideBar/SideBar'
import './style.css'
// import TopNavBar from './TopNavBar/TopNavBar'

export default function Layout({ title, children }) {
    const { token } = useAuthState();
    return (
        <>
            {token ?
                <div className="wrapper">
                    <SideBar title={title} />
                    <div className="main">
                        {/* <TopNavBar /> */}
                        {/* Main */}
                        <main className="content">
                            <div className="container-fluid p-0">
                                <h5 className="h4 mb-3">{title}</h5>
                                <div className="row">
                                    <div className="col-12">
                                        {children}
                                    </div>
                                </div>
                            </div>
                        </main>
                        <Footer />
                    </div>
                </div>
                : <Navigate to="/login" replace />}
        </>
    )
}