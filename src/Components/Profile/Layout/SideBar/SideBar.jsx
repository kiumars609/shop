import React from 'react'
import { Link } from 'react-router-dom';
import { User_loggedin } from '../../../../Context/User_loggedin';
import handleLoggedOut from '../../../../Context/ActionLogOut'
import '../style.css'

export default function SideBar({ title }) {

    const userInfo = User_loggedin()
    const exit = handleLoggedOut()
    const menuLinks = [
        { title: 'Dashboard', link: '/profile' },
        { title: 'Orders', link: '/profile/orders' },
    ]
    const mapMenuLinks = menuLinks && menuLinks.map((item, index) => {
        return (
            <li key={index} className={`sidebar-item ${title === item.title && 'active'}`}>
                <Link to={item.link} className="sidebar-link">
                    <i className="align-middle" data-feather="sliders"></i> <span className="align-middle">{item.title}</span>
                </Link>
            </li>
        )
    })
    return (
        <>
            <nav id="sidebar" className="sidebar js-sidebar">
                <div className="sidebar-content js-simplebar">
                    <Link to={'/'} className="sidebar-brand">
                        <span className="align-middle">
                            <img src='/assets/main/images/logo/logo4.png' alt='logo' />
                        </span>
                    </Link>
                    <div className="col-md-12 col-xl-11 mx-auto mt-3">
                        <div className="card mb-3">
                            <div className="card-header">
                                {/* <h5 className="card-title mb-0">Profile Details</h5> */}
                            </div>
                            <div className="card-body text-center">
                                <img src={`/assets/main/images/users/avatar/${userInfo.avatar}`} alt="Christina Mason" className="img-fluid rounded-circle mb-2" width="128" height="128" />
                                <h5 className="card-title mb-0">{userInfo.fname + ' ' + userInfo.lname}</h5>
                                {/* <div className="text-muted mb-2">Lead Developer</div> */}
                                <div>
                                    <a className="mt-3" href="true">
                                        <i className='fa fa-edit fs-5'></i>
                                    </a>
                                    {/* <a className="btn btn-primary btn-sm" href="true"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" className="feather feather-message-square"><path d="M21 15a2 2 0 0 1-2 2H7l-4 4V5a2 2 0 0 1 2-2h14a2 2 0 0 1 2 2z"></path></svg> Message</a> */}
                                </div>
                            </div>
                        </div>
                    </div>
                    <ul className="sidebar-nav">
                        {mapMenuLinks}
                        <li className="sidebar-item">
                            <span className="sidebar-link" role='link' onClick={exit}>
                                <i className="align-middle" data-feather="book"></i> <span className="align-middle">Logout</span>
                            </span>
                        </li>
                    </ul>
                </div>
            </nav>
        </>
    )
}
