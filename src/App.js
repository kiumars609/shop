import React from "react";
import "./App.css";
import HomePage from "./Pages/HomePage/HomePage";
import ProductDetail from "./Pages/ProductDetail/ProductDetail";
import Error404 from "./Pages/Error404/Error404";
import Cart from "./Pages/Cart/Cart";
import { Routes, Route } from "react-router-dom";
import { useScrollToTop } from "./CustomHooks/useScrollToTop";
import CartItems from "./CustomHooks/CartItems";
import CheckOut from "./Pages/CheckOut/CheckOut";
import PaySuccess from "./Pages/Paying/PayingSuccess/PaySuccess";
import Shop from "./Pages/Shop";
import Compare from "./Pages/Compare";
import AboutUs from "./Pages/AboutUs/AboutUs";
import ContactUs from "./Pages/ContactUs/ContactUs";
import Blog from "./Pages/Blog/Blog";
import BlogDetail from "./Pages/Blog/BlogDetail/BlogDetail";
import Tag from "./Pages/Blog/Tag/Tag";
import Category from "./Pages/Blog/Category/Category";
import Dashboard from "./Pages/UserPanel/Dashboard/Dashboard";
import Login from "./Pages/UserPanel/Login/Login";
import Register from "./Pages/UserPanel/Login/Register";
import Orders from "./Pages/UserPanel/Orders/Orders";
import HomePageBeta from "../src/Pages/NewVersionBeta/HomePage";
import Invoice from "./Pages/UserPanel/Orders/Invoice";
import Products from "./Pages/NewVersionBeta/Products/Products";

function App() {
  const cart = CartItems();
  useScrollToTop();
  return (
    <>
      <Routes>
        <Route
          path="/"
          exact
          element={<HomePage cartItems={cart.cartItems} />}
        />
        <Route
          path="/product/:category/:id"
          element={
            <ProductDetail
              cartItems={cart.cartItems}
              handleAddProduct={cart.handleAddProduct}
            />
          }
        />
        <Route
          path="/cart"
          element={
            <Cart
              cartItems={cart.cartItems}
              handleAddProduct={cart.handleAddProduct}
              handleRemoveProduct={cart.handleRemoveProduct}
              handleCartClearance={cart.handleCartClearance}
            />
          }
        />
        <Route
          path="/checkout"
          element={<CheckOut cartItems={cart.cartItems} />}
        />
        <Route
          path="/paying/success"
          element={<PaySuccess cartItems={cart.cartItems} />}
        />
        <Route
          path="/shop/:category"
          element={<Shop cartItems={cart.cartItems} />}
        />
        <Route
          path="/compare/:category/:id"
          element={<Compare cartItems={cart.cartItems} />}
        />
        <Route
          path="/checkout/:type/:category/:id/:quantity"
          element={<CheckOut cartItems={cart.cartItems} />}
        />
        <Route
          path="/about-us"
          element={<AboutUs cartItems={cart.cartItems} />}
        />
        <Route
          path="/contact-us"
          element={<ContactUs cartItems={cart.cartItems} />}
        />
        {/* Blog Routes */}
        <Route path="/blog" element={<Blog cartItems={cart.cartItems} />} />
        <Route
          path="/blog/:id"
          element={<BlogDetail cartItems={cart.cartItems} />}
        />
        <Route
          path="/blog/tag/:tagname"
          element={<Tag cartItems={cart.cartItems} />}
        />
        <Route
          path="/blog/category/:catname"
          element={<Category cartItems={cart.cartItems} />}
        />
        {/* User */}
        <Route path="/login" element={<Login cartItems={cart.cartItems} />} />
        <Route
          path="/register"
          element={<Register cartItems={cart.cartItems} />}
        />
        <Route path="/profile" element={<Dashboard />} />
        <Route path="/profile/orders" element={<Orders />} />
        <Route path="/profile/invoice/:id" element={<Invoice />} />

        {/* Beta */}
        <Route path="/beta" element={<HomePageBeta />} />
        <Route path="/beta/products/:proname" element={<Products />} />

        {/* 404 Rout */}
        <Route path="/*" element={<Error404 />} />
      </Routes>
    </>
  );
}

export default App;
