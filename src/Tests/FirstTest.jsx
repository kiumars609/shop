import React from "react";

export default function FirstTest({ name }) {
  return (
    <div>
      <p>{name ? `Hello ${name}` : "Hello"}</p>
    </div>
  );
}
