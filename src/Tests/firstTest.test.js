import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import FirstTest from "./FirstTest";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

describe("Show Hello Message", () => {
  test("Without Name", () => {
    act(() => {
      render(<FirstTest />, container);
    });

    expect(container.textContent).toBe("Hello");
  });

  test("With Name", () => {
    act(() => {
      render(<FirstTest name={"Kiumars"} />, container);
    });

    expect(container.textContent).toBe("Hello Kiumars");
  });
});
