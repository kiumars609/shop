import { useState, useEffect } from 'react';
import { get } from '../services';

export function User_loggedin() {
  const token = localStorage.getItem('token')
  const [data, setData] = useState('')
  const [userInfo, setUserInfo] = useState('')

  useEffect(() => {
    get('/users/getUserAPI.php')
      .then(response => {
        setData(response)
      })
  }, [token]);

  useEffect(() => {
    if (data) {
      const filter = data.filter(item => item.tokens === token)
      setUserInfo(filter[0])
    }
  }, [data, token]);
  return userInfo
}
