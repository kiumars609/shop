import { useAuthDispatcher } from './auth-context'
import { actionType } from './reducer'

export default function ActionLogOut() {
    const dispatch = useAuthDispatcher()
    const handleLoggedOut = () => {
        dispatch({
            type: actionType.LOGOUT,
        })
        localStorage.removeItem('token')
        window.location.replace("/");
    }
    return handleLoggedOut
}
