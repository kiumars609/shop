import React from 'react'
import { Link } from 'react-router-dom'
import '../sass/css/main.css'

export default function HomePage() {
    return (
        <>
            <div className="col-md-12 back-shop p-0 beta">
                <div className="col-md-12 mx-auto p-0 header text-center position-relative">
                    <h1 className='text-white pt-4 title'>Shop</h1>
                    <Link to={'/'}>
                        <img src='/beta/image/logo/logo3.png' className='col-md-2 logo' alt='logo' />
                    </Link>
                    <div className="cart">
                        <input type="checkbox" id="sidebar-toggle-input" />
                        <label className="sidebar-toggle" htmlFor="sidebar-toggle-input">
                            <img src='/beta/image/icon/tabletCartSmall.png' className='col-md-12' alt='cart' />
                        </label>
                        <div className="sidebar">
                            <img src='/beta/image/icon/tabletCartBig.png' className='col-md-12' alt='cart' />
                        </div>
                    </div>




                </div>


                <div className="col-md-4 bg-warning advertise-box">
                    <div id="carouselExampleControls" className="carousel slide" data-bs-ride="carousel">
                        <div className="carousel-inner">
                            <div className="carousel-item active">
                                <img src="https://gamefa.com/wp-content/uploads/2022/11/PS547.jpg.webp" className="d-block w-100" height="300" alt="..." />
                            </div>
                            <div className="carousel-item">
                                <img src="https://gamefa.com/wp-content/uploads/2022/12/AliceDarling-768x432.jpg.webp" className="d-block w-100" height="300" alt="..." />
                            </div>
                            <div className="carousel-item">
                                <img src="https://gamefa.com/wp-content/uploads/2022/12/b6a0af109140859cb49ff21582a122eb26ee233a-768x432.jpg" className="d-block w-100" height="300" alt="..." />
                            </div>
                        </div>
                        <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                            <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span className="visually-hidden">Previous</span>
                        </button>
                        <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                            <span className="carousel-control-next-icon" aria-hidden="true"></span>
                            <span className="visually-hidden">Next</span>
                        </button>
                    </div>
                </div>


                <div className="row mx-auto">
                    <div className="col-md-6 left-door">
                        <img src='/beta/image/logo/left-logo.png' className='col-md-2' alt='logo' />
                    </div>
                    <div className="col-md-6 right-door">
                        <img src='/beta/image/logo/right-logo.png' className='col-md-2' alt='logo' />
                    </div>
                </div>

                <div className="right-arrow">
                    <Link to={''} className='borad-link'><div className="board-r-electronic">Electronics</div></Link>
                    <Link to={''}><div className="board-l-cloth">Clothes</div></Link>
                    <Link to={''}><div className="board-r-beuty-health">Beauty & Health</div></Link>
                </div>

                <div className="left-arrow">
                    <Link to={''}><div className="board-r-electronic">Electronics</div></Link>
                    <Link to={''}><div className="board-l-cloth">Clothes</div></Link>
                    <Link to={''}><div className="board-r-beuty-health">Beauty & Health</div></Link>
                </div>





            </div>

        </>
    )
}
