import React from "react";
import { Link } from "react-router-dom";
import "./scss/main.css";

export default function Products() {
  return (
    <>
      <div className="col-md-12 back-shop p-0 beta products">
        {/* Start Header Section */}
        <div className="col-md-12 mx-auto p-0 header text-center position-relative">
          <h1 className="text-white pt-4 title">Shop</h1>
          <Link to={"/"}>
            <img
              src="/beta/image/logo/logo3.png"
              className="col-md-2 logo"
              alt="logo"
            />
          </Link>
          <div className="cart">
            <input type="checkbox" id="sidebar-toggle-input" />
            <label className="sidebar-toggle" htmlFor="sidebar-toggle-input">
              <img
                src="/beta/image/icon/tabletCartSmall.png"
                className="col-md-12"
                alt="cart"
              />
            </label>
            <div className="sidebar">
              <img
                src="/beta/image/icon/tabletCartBig.png"
                className="col-md-12"
                alt="cart"
              />
            </div>
          </div>
        </div>
        {/* End Header Section */}

        <div className="col-md-8 main-products px-3">
            <div className="col-md-12 bg-white d-flex">
                <div className="col-4 card card-product">
                  <div className="col-10 bg-warning product">1</div>
                  <img src='/beta/image/products/shelve.png' className="col-12" />
                </div>

                <div className="col-4 card card-product">
                  
                  <img src='/beta/image/products/shelve.png' className="col-12" />
                </div>

                <div className="col-4 card card-product">
                  
                  <img src='/beta/image/products/shelve.png' className="col-12" />
                </div>
            </div>
        </div>

        <div className="col-md-4 d-flex align-items-center main-navs">
            <div className="col-6 bg-danger top">1</div>
            <div className="col-6 bg-info bottom">2</div>
        </div>

      </div>
    </>
  );
}
