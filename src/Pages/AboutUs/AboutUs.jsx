import React from 'react'
import Layout from '../../Components/Shop/Layout/Layout'
import './style.css'

export default function AboutUs({ cartItems }) {
    return (
        <>
            <Layout cartItems={cartItems} pgTitle='About Us'>
                <section className="about-us section">
                    <div className="container">
                        <div className="row align-items-center">
                            <div className="col-lg-6 col-md-12 col-12">
                                <div className="content-left">
                                    <img src="/assets/main/images/about/who-we-are-tech.jpg" alt="#" />
                                </div>
                            </div>
                            <div className="col-lg-6 col-md-12 col-12">
                                <div className="content-right">
                                    <h2>who are we ?</h2>
                                    <p>We are a technology-based platform whose mission is to create a pleasant experience of buying and selling for all Iranian people.
                                        As the most popular online store in Iran and the region, we are on the way to becoming the first online shopping and selling authority,
                                        and for this purpose, providing services in accordance with global standards is our most important task.
                                        DigiWare's identity is more than anything influenced by our audience and we take steps based on their needs.
                                        For this reason, customer-centricity is the most important value of our business in DigiWare Group.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="about-us section bg-white counter">
                    <div className="container">
                        <h5 className='about-title mb-5'>DigiWare at a glance</h5>
                        <div className="row align-items-center">
                            <div className="column">
                                <div className="card">
                                    <p><i className="fa fa-eye"></i></p>
                                    <h3 className='mt-2'>40,000,000</h3>
                                    <p>Monthly Unique Visitor</p>
                                </div>
                            </div>
                            <div className="column">
                                <div className="card">
                                    <p><i className="fa fa-shop"></i></p>
                                    <h3 className='mt-2'>+200,000</h3>
                                    <p>Seller</p>
                                </div>
                            </div>
                            <div className="column">
                                <div className="card">
                                    <p><i className="fa fa-cart-plus"></i></p>
                                    <h3 className='mt-2'>+7,500,000</h3>
                                    <p>Product Variety</p>
                                </div>
                            </div>
                            <div className="column">
                                <div className="card">
                                    <p><i className="fa fa-users"></i></p>
                                    <h3 className='mt-2'>+8,700</h3>
                                    <p>DigiWare Colleague</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="about-us section">
                    <div className="container">
                        <div className="row align-items-center">
                            <div className="col-lg-6 col-md-12 col-12">
                                <div className="content-right">
                                    <h2>what are we doing ?</h2>
                                    <p>Focusing on the digital economy is the common feature of all active services and products in the DigiWare Group.
                                        DigiWare Market Place is known as the core of the group's activities and is a reliable platform for buying and selling.
                                        In addition to e-commerce, which is the main part of our business,
                                        the activities of DigiWare group members continue in various other fields:
                                        marketing technology, financial technology, logistics, content, social responsibility and innovation.</p>
                                </div>
                            </div>
                            <div className="col-lg-6 col-md-12 col-12">
                                <div className="content-left">
                                    <img src="/assets/main/images/about/what-we-do-new.jpg" alt="#" />
                                </div>
                            </div>

                        </div>
                    </div>
                </section>
                <section className="about-us section bg-white">
                    <div className="container">
                        <div className="row align-items-center">
                            <div className="col-lg-6 col-md-12 col-12">
                                <div className="content-left">
                                    <img src="/assets/main/images/about/working-at-digiware-new.jpg" alt="#" />
                                </div>
                            </div>
                            <div className="col-lg-6 col-md-12 col-12">
                                <div className="content-right">
                                    <h2>work in DigiWare</h2>
                                    <p>We believe that DigiWare is defined by its human capital, i.e. more than 9,000 colleagues.
                                        As an organization with very fast growth, we face various issues in terms of decision-making, recruiting and hiring,
                                        performance evaluation and development of people,
                                        but with the agility of processes, providing facilities and facilities in accordance with global standards and in line with top
                                        technology-oriented businesses. world, we strive to ensure mutual success for businesses and individuals. Working at DigiWare is
                                        like swimming in a vast and deep ocean, full of challenges and learning.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </Layout>
        </>
    )
}