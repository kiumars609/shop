import React from 'react'
import Pdf from "react-to-pdf";
import ReactToPrint from 'react-to-print';

export default function PaySuccessBottomButtons({ back }) {
    return (
        <>
            <div className="row mx-auto mt-4 col-md-7">
                <Pdf targetRef={back.ref} filename={`receipt-${back.item['newItems']['transId']}.pdf`}>
                    {({ toPdf }) =>
                        <div className="col-4"><i className="lni lni-adobe"></i> <span role='button' className='bottom-links' onClick={toPdf}>Save as PDF</span></div>
                    }
                </Pdf>
                <ReactToPrint
                    trigger={() => <div className="col-4"><i className="lni lni-printer"></i> <span role='button' className='bottom-links'>Print Receipt</span></div>}
                    content={() => back.ref.current}
                />
                <div className="col-4"><i className="lni lni-envelope"></i> <span role='button' className='bottom-links'>Email Receipt</span></div>
            </div>
        </>
    )
}
