import React from 'react'
import Layout from '../../../Components/Shop/Layout/Layout'
import PaySuccessBottomButtons from './PaySuccessBottomButtons';
import PaySuccessDataRow from './PaySuccessDataRow';
import Tostify from '../../../Components/Tostify';
import { Backend } from './Backend';
import { Link } from 'react-router-dom';
import './style.css'

export default function PaySuccess({ cartItems }) {
  const back = Backend()
  return (
    <>
      <Layout cartItems={cartItems} pgTitle={`Receipt-${back.item['newItems']['transId']}`}>
        <Tostify position='top-center' width='500px' />
        <div className="row mb-5" >
          <div className="col-8">
            <div className="col-10 bg-white mx-auto mt-5 p-4" ref={back.ref}>
              <h4 className='text-success'>Payment successful</h4>
              <p className='pt-2'>
                Thank you for choosing DigiWare. Your payment was successful!
              </p>
              <PaySuccessDataRow cartItems={cartItems} item={back.item} />
            </div>
            <PaySuccessBottomButtons back={back} />
          </div>
          <div className="col-4 text-center">
            <img src='/assets/main/images/payment/success.png' alt='' />
            <div className="col-md-12 text-center mt-5 button">
              <Link to='/' className='btn col-md-6'>Back Home</Link>
            </div>
          </div>
        </div>
      </Layout>
    </>
  )
}
