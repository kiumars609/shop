import React from 'react'
import { toast } from 'react-toastify';
import { useLocation } from 'react-router-dom';

export function Backend() {
    const ref = React.createRef();
    let locat = useLocation()
    const item = locat.state;
    const customId = "custom-id-yes";
    toast['success']('Thank you! Your payment is complete', {
        toastId: customId
    })
    return { ref, locat, item }
}
