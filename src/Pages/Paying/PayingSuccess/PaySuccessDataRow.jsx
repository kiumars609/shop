import React from 'react'
import { Link } from 'react-router-dom'
import { useCommaSeparators } from '../../../CustomHooks/useCommaSeparators'

export default function PaySuccessDataRow({ cartItems, item }) {

    // console.log(item['newItems']['productsInfo']);

    const p = item['newItems']['productsInfo']

    const mapCarts = p && p.map((items, index) => {
        return (
            <span key={index}>
                <img src={`/assets/main/images/products/${items.category}/${items.img1}`} width="30" alt={items.titleSmall} />
                <Link target='_blank' to={`/product/${items.category}/${items.id}`} className="btn new-link"> {items.titleSmall}</Link>
            </span>
        )
    })
    return (
        <>
            <div className="row mt-5">
                <div className="col-6">Transactions ID</div>
                <div className="col-6 result">{item['newItems']['transId']}</div>
                <hr className='mt-3' />
                <div className="col-6">Date</div>
                <div className="col-6 result">{item['newItems']['datePaying']}</div>
                <hr className='mt-3' />
                <div className="col-6">Mode of Payment</div>
                <div className="col-6 result">PayPal</div>
                <hr className='mt-3' />
                <div className="col-6">Mode of Shipping</div>
                <div className="col-6 result">{item['newItems']['shipType']}</div>
                <hr className='mt-3' />
                <div className="col-6">Customer Name</div>
                <div className="col-6 result">{item.firstName + ' ' + item.lastName}</div>
                <hr className='mt-3' />
                <div className="col-6">Mobile No</div>
                <div className="col-6 result">{item.phone}</div>
                <hr className='mt-3' />
                <div className="col-6">Email</div>
                <div className="col-6 result">{item.email}</div>
                <hr className='mt-3' />
                <div className="col-6">Country</div>
                <div className="col-6 result">{item.country}</div>
                <hr className='mt-3' />
                <div className="col-6">City</div>
                <div className="col-6 result">{item.city}</div>
                <hr className='mt-3' />
                <div className="col-6">Address</div>
                <div className="col-6 result">{item.address}</div>
                <hr className='mt-3' />
                <div className="col-6">PostCode</div>
                <div className="col-6 result">{item.postCode}</div>
                <hr className='mt-3' />
                <div className="col-6">Product(s)</div>
                <div className="col-6 result products">{mapCarts}</div>
                <hr className='mt-3' />
                <div className="col-6">Payment Amount</div>
                <div className="col-6 result">€{useCommaSeparators(item['newItems']['totalPrice'])}</div>
                <hr className='mt-3' />
            </div>
        </>
    )
}
