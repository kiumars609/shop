import React from 'react'

export default function Reviews() {
    return (
        <>
            <div className="col-lg-8 col-12">
                <div className="single-block">
                    <div className="reviews">
                        <h4 className="title">Latest Reviews</h4>
                        <div className="single-review">
                            <img src="/assets/main/images/blog/comment1.jpg" alt="#" />
                            <div className="review-info">
                                <h4>Awesome quality for the price
                                    <span>Jacob Hammond
                                    </span>
                                </h4>
                                <ul className="stars">
                                    <li><i className="lni lni-star-filled"></i></li>
                                    <li><i className="lni lni-star-filled"></i></li>
                                    <li><i className="lni lni-star-filled"></i></li>
                                    <li><i className="lni lni-star-filled"></i></li>
                                    <li><i className="lni lni-star-filled"></i></li>
                                </ul>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                    tempor...</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
