import React from 'react'

export default function Stars() {
    return (
        <>
            <div className="col-lg-4 col-12">
                <div className="single-block give-review">
                    <h4>4.5 (Overall)</h4>
                    <ul>
                        <li>
                            <span>5 stars - 38</span>
                            <i className="lni lni-star-filled"></i>
                            <i className="lni lni-star-filled"></i>
                            <i className="lni lni-star-filled"></i>
                            <i className="lni lni-star-filled"></i>
                            <i className="lni lni-star-filled"></i>
                        </li>
                        <li>
                            <span>4 stars - 10</span>
                            <i className="lni lni-star-filled"></i>
                            <i className="lni lni-star-filled"></i>
                            <i className="lni lni-star-filled"></i>
                            <i className="lni lni-star-filled"></i>
                            <i className="lni lni-star"></i>
                        </li>
                        <li>
                            <span>3 stars - 3</span>
                            <i className="lni lni-star-filled"></i>
                            <i className="lni lni-star-filled"></i>
                            <i className="lni lni-star-filled"></i>
                            <i className="lni lni-star"></i>
                            <i className="lni lni-star"></i>
                        </li>
                        <li>
                            <span>2 stars - 1</span>
                            <i className="lni lni-star-filled"></i>
                            <i className="lni lni-star-filled"></i>
                            <i className="lni lni-star"></i>
                            <i className="lni lni-star"></i>
                            <i className="lni lni-star"></i>
                        </li>
                        <li>
                            <span>1 star - 0</span>
                            <i className="lni lni-star-filled"></i>
                            <i className="lni lni-star"></i>
                            <i className="lni lni-star"></i>
                            <i className="lni lni-star"></i>
                            <i className="lni lni-star"></i>
                        </li>
                    </ul>

                    <button type="button" className="btn review-btn" data-bs-toggle="modal"
                        data-bs-target="#exampleModal">
                        Leave a Review
                    </button>
                </div>
            </div>
        </>
    )
}
