import React from 'react'
import Reviews from './Reviews'
import Stars from './Stars'

export default function ProductDetailBottomArea() {
    return (
        <>
            <div className="row">
                <Stars />
                <Reviews />
            </div>
        </>
    )
}
