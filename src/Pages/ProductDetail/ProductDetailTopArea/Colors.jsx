import React, { useEffect } from 'react'
import $ from 'jquery';

export default function Colors({ data, handleColor }) {

    useEffect(() => {
        $('.product-list').on('change', function () {
            $('.product-list').not(this).prop('checked', false)
        })
    }, []);
    const array = JSON.parse("[" + data.color + "]");

    const dataMap = array.map((itm, index) => {
        return (
            <div key={index} className={`single-checkbox checkbox-style-${itm} text-center`}>
                {index === 0 ?
                    <input type="checkbox" id={`checkbox-${itm}`} name="myCheckbox" defaultChecked={true} onChange={(e) => handleColor(e)} className='product-list' value={itm} />
                    :
                    <input type="checkbox" id={`checkbox-${itm}`} name="myCheckbox" className='product-list' onChange={(e) => handleColor(e)} value={itm} />
                }
                <label htmlFor={`checkbox-${itm}`}><span></span></label>
                <br />
                {itm === 1 && <label className='clr' htmlFor={`checkbox-${itm}`}>Black</label>}
                {itm === 2 && <label className='clr' htmlFor={`checkbox-${itm}`}>Blue</label>}
                {itm === 3 && <label className='clr' htmlFor={`checkbox-${itm}`}>Red</label>}
                {itm === 4 && <label className='clr' htmlFor={`checkbox-${itm}`}>Green</label>}
            </div>
        )
    })
    return (
        <>
            {dataMap}
        </>
    )
}
