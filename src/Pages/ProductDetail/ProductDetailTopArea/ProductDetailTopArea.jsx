import React from 'react'
import { setFeatures } from '../setFeatures'
import useNoImage from '../../../CustomHooks/useNoImage'
import Colors from './Colors'
import { useCommaSeparators } from '../../../CustomHooks/useCommaSeparators';
import '../style.css'
import { Link } from 'react-router-dom';

export default function ProductDetailTopArea({ item, handleAddProduct, handleQuantity, quantity }) {

    const image1 = `/assets/main/images/products/` + item.category + `/` + item.img1;
    const image2 = `/assets/main/images/products/` + item.category + `/` + item.img2;
    const image3 = `/assets/main/images/products/` + item.category + `/` + item.img3;
    const image4 = `/assets/main/images/products/` + item.category + `/` + item.img4;
    const image5 = `/assets/main/images/products/` + item.category + `/` + item.img5;

    const handleColor = (e) => {
        console.log(e.target.value);
    }
    return (
        <>
            <div className="top-area">
                <div className="row align-items-center">
                    {/* Slider */}
                    <div className="col-lg-6 col-md-12 col-12">
                        <div className="product-images">
                            <div className="container">
                                <div className="row d-flex justify-content-center">
                                    <div className="col-md-12 p-0">
                                        <div id="myCarousel" className="carousel slide position-relative" data-bs-ride="carousel" align="center">
                                            <Link to={`/compare/laptop/${item.id}`} className='compare-icon'><i className="fa-solid fa-code-compare"></i></Link>
                                            <div className="carousel-inner">
                                                <div className="carousel-item active"> <img src={useNoImage(image1)} className="rounded" alt={item.titleSmall} /> </div>
                                                <div className="carousel-item"> <img src={useNoImage(image2)} className="rounded" alt={item.titleSmall} /> </div>
                                                <div className="carousel-item"> <img src={useNoImage(image3)} className="rounded" alt={item.titleSmall} /> </div>
                                                <div className="carousel-item"> <img src={useNoImage(image4)} className="rounded" alt={item.titleSmall} /> </div>
                                                <div className="carousel-item"> <img src={useNoImage(image5)} className="rounded" alt={item.titleSmall} /> </div>
                                            </div>
                                            <ol className="carousel-indicators list-inline">
                                                <li className="list-inline-item active"> <a href="true" id="carousel-selector-0" className="selected" data-bs-slide-to="0" data-bs-target="#myCarousel"> <img src={useNoImage(image1)} className="img-fluid rounded" alt={item.titleSmall} /> </a> </li>
                                                <li className="list-inline-item"> <a href="true" id="carousel-selector-1" data-bs-slide-to="1" data-bs-target="#myCarousel"> <img src={useNoImage(image2)} alt={item.titleSmall} /> </a> </li>
                                                <li className="list-inline-item"> <a href="true" id="carousel-selector-2" data-bs-slide-to="2" data-bs-target="#myCarousel"> <img src={useNoImage(image3)} className="img-fluid rounded" alt={item.titleSmall} /> </a> </li>
                                                <li className="list-inline-item"> <a href="true" id="carousel-selector-2" data-bs-slide-to="3" data-bs-target="#myCarousel"> <img src={useNoImage(image4)} className="img-fluid rounded" alt={item.titleSmall} /> </a> </li>
                                                <li className="list-inline-item"> <a href="true" id="carousel-selector-2" data-bs-slide-to="4" data-bs-target="#myCarousel"> <img src={useNoImage(image5)} className="img-fluid rounded" alt={item.titleSmall} /> </a> </li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* Slider */}
                    <div className="col-lg-6 col-md-12 col-12">
                        <div className="product-info">
                            <h2 className="title">{item.title}</h2>
                            <p className="category"><i className="lni lni-tag"></i> Category:<Link to={`/shop/${item.category}`}>{item.category}</Link></p>
                            {/* <h3 className="price">$850<span>$945</span></h3> */}
                            <h3 className="price">€{useCommaSeparators(item.price)}</h3>
                            <h5>Features</h5>
                            {setFeatures(item)}
                            <div className="row">
                                <div className="col-lg-4 col-md-4 col-12">
                                    <div className="form-group color-option">
                                        <label className="title-label" htmlFor="size">Choose color</label>
                                        <Colors data={item} handleColor={handleColor} />
                                    </div>
                                </div>
                                <div className="col-lg-4 col-md-4 col-12">
                                    <div className="form-group">
                                        <label htmlFor="color">Battery capacity</label>
                                        <select className="form-control" id="color">
                                            <option>5100 mAh</option>
                                            <option>6200 mAh</option>
                                            <option>8000 mAh</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="col-lg-4 col-md-4 col-12">
                                    <div className="form-group quantity">
                                        <label htmlFor="color">Quantity</label>
                                        <select className="form-control" onChange={(e) => handleQuantity(e)}>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div className="bottom-content">
                                <div className="row align-items-end">
                                    <div className="col-lg-6 col-md-6 col-12">
                                        <div className="button cart-button">
                                            <button className="btn" style={{ width: '100%' }} onClick={() => handleAddProduct(item)} data-bs-toggle="modal" data-bs-target="#cardModal">Add to Basket</button>
                                        </div>
                                    </div>
                                    <div className="col-lg-6 col-md-6 col-12">
                                        <div className="button cart-button">
                                            <Link to={`/checkout/single/${item.category}/${item.id}/${quantity}`} className="btn buy-now-button col-md-12">Buy Now</Link>
                                        </div>
                                    </div>
                                    {/* <div className="col-lg-4 col-md-4 col-12">
                                        <div className="wish-button">
                                            <button className="btn"><i className="lni lni-reload"></i> Compare</button>
                                        </div>
                                    </div>
                                    <div className="col-lg-4 col-md-4 col-12">
                                        <div className="wish-button">
                                            <button className="btn"><i className="lni lni-heart"></i> To Wishlist</button>
                                        </div>
                                    </div> */}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {/* <!-- Modal --> */}
        </>
    )
}
