import React, { useEffect } from 'react'
import { useState } from 'react'
import { useParams } from 'react-router-dom'
import Loading from '../../Components/Loading'
import Layout from '../../Components/Shop/Layout/Layout'
import { get } from '../../services'
import ProductDetailBottomArea from './ProductDetailBottomArea/ProductDetailBottomArea'
import ProductDetailInsertReview from './ProductDetailInsertReview'
import ProductDetailPageInfo from './ProductDetailPageInfo'
import ProductDetailInfo from './ProductDetailInfo'
import ProductDetailTopArea from './ProductDetailTopArea/ProductDetailTopArea'
import './style.css'

export default function ProductDetail({ handleAddProduct, cartItems }) {

  const item = useParams()
  const [data, setData] = useState('')
  const [quantity, setQuantity] = useState(1)
  const [loader, setLoader] = useState(true);

  useEffect(() => {
    get(`/products/${item.category}/${item.category}API.php`)
      .then(response => {
        for (let index = 0; index < response.length; index++) {
          if (response[index].id === item.id) {
            const element = response[index];
            setData(element)
            setLoader(false)
          }
        }
      })
      .catch(function (error) {
        console.log(error);
      })
      .then(function () {
        // always executed
      });
  }, [item]);

  const handleQuantity = (e) => {
    setQuantity(e.target.value)
  }

  


  return (
    <>
      <Layout pgTitle={data.titleSmall} cartItems={cartItems}>
        {loader ? <Loading /> :
          <>
            <ProductDetailPageInfo item={data} />
            <section className="item-details section">
              <div className="container">
                <ProductDetailTopArea handleAddProduct={handleAddProduct} item={data} handleQuantity={handleQuantity} quantity={quantity} />
                <div className="product-details-info">
                  <ProductDetailInfo item={data} />
                  <ProductDetailBottomArea />
                </div>
              </div>
            </section>
            <ProductDetailInsertReview />
          </>
        }
      </Layout>
    </>
  )
}
