import React from 'react'
import { setSpecification } from './setSpecification'

export default function ProductDetailInfo({ item }) {
    return (
        <>
            <div className="single-block">
                <div className="row">

                    <ul className="nav nav-tabs" id="myTab" role="tablist">
                        <li className="nav-item" role="presentation">
                            <button className="nav-link active" id="introduction-tab" data-bs-toggle="tab" data-bs-target="#introduction" type="button" role="tab" aria-controls="introduction" aria-selected="true">Introduction</button>
                        </li>
                        <li className="nav-item" role="presentation">
                            <button className="nav-link" id="specifications-tab" data-bs-toggle="tab" data-bs-target="#specifications" type="button" role="tab" aria-controls="specifications" aria-selected="false">Specifications</button>
                        </li>
                    </ul>
                    <div className="tab-content" id="myTabContent">
                        <div className="tab-pane fade show active" id="introduction" role="tabpanel" aria-labelledby="introduction-tab">
                            <div className="info-body mt-4">
                                <div className="info-body custom-responsive-margin">
                                    <h4>Introduction</h4>
                                    <p>{item.description}</p>
                                </div>
                            </div>
                        </div>
                        <div className="tab-pane fade" id="specifications" role="tabpanel" aria-labelledby="specifications-tab">
                            <div className="info-body mt-4">
                                <h4>Specifications</h4>
                                {setSpecification(item)}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
