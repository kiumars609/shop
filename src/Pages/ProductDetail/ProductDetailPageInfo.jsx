import React from 'react'
import { Link } from 'react-router-dom'
import './style.css'

export default function ProductDetailPageInfo({ item }) {
    return (
        <>
            <div className="breadcrumbs">
                <div className="container">
                    <div className="row align-items-center">
                        <div className="col-lg-6 col-md-6 col-12">
                            <div className="breadcrumbs-content">
                                <h1 className="page-title">{item.titleSmall}</h1>
                            </div>
                        </div>
                        <div className="col-lg-6 col-md-6 col-12">
                            <ul className="breadcrumb-nav">
                                <li><Link to='/'><i className="lni lni-home"></i> Home</Link></li>
                                <li><Link to={`/shop/${item.category}`}>{item.category}</Link></li>
                                <li>{item.titleSmall}</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
