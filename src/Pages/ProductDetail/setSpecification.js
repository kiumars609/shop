export function setSpecification(item) {
    if (item.category === 'laptop') {
        return (
            <ul className="features">
                <li><span>Weight:</span> <span className="text-dark">{item.weight}Kg</span></li>
                <li><span>Processor Model:</span> <span className="text-dark">{item.processorModel}</span></li>
                <li><span>Processor Speed Range:</span> <span className="text-dark">{item.processorSpeedRange}</span></li>
                <li><span>Cache:</span> <span className="text-dark">{item.cache}</span></li>
                <li><span>Processor Frequency:</span> <span className="text-dark">{item.processorFrequency}</span></li>
                <li><span>Internal Memory Capacity:</span> <span className="text-dark">{item.internalMemoryCapacity}</span></li>
                <li><span>Internal Memory Type:</span> <span className="text-dark">{item.internalMemoryType}</span></li>
                <li><span>Internal Memory Specifications:</span> <span className="text-dark">{item.internalMemorySpecifications}</span></li>
                <li><span>GPU Model:</span> <span className="text-dark">{item.GPUModel}</span></li>
                <li><span>GPU Dedicated Memory:</span> <span className="text-dark">{item.GPUDedicatedMemory}</span></li>
                <li><span>Screen Type:</span> <span className="text-dark">{item.screenType}</span></li>
                <li><span>Device Capabilities:</span> <span className="text-dark">{item.deviceCapabilities}</span></li>
                <li><span>Optical Drive:</span> <span className="text-dark">{item.opticalDrive}</span></li>
                <li><span>Touchpad Specifications:</span> <span className="text-dark">{item.touchpadSpecifications}</span></li>
                <li><span>Communication Ports:</span> <span className="text-dark">{item.communicationPorts}</span></li>
                <li><span>Number of USB 3.0 Ports:</span> <span className="text-dark">{item.numberOfUSB3Ports}</span></li>
                <li><span>Battery Type:</span> <span className="text-dark">{item.batteryType}</span></li>
                <li><span>Battery Description:</span> <span className="text-dark">{item.batteryDescription}</span></li>
                <li><span>Battery Charging:</span> <span className="text-dark">{item.batteryCharging}</span></li>
                <li><span>OS:</span> <span className="text-dark">{item.OS}</span></li>
                <li><span>Included Items:</span> <span className="text-dark">{item.includedItems}</span></li>
            </ul>
        )
    }
    if (item.category === 'mobile') {
        return (
            <ul className="features">
                <li><span>Weight:</span> <span className="text-dark">{item.weight}g</span></li>
                <li><span>Screen Size Range:</span> <span className="text-dark">{item.screenSizeRange}</span></li>
                <li><span>Sim Card:</span> <span className="text-dark">{item.simCard}</span></li>
                <li><span>Sim Card Number:</span> <span className="text-dark">{item.simCardNumber}</span></li>
                <li><span>Resolution:</span> <span className="text-dark">{item.resolution}</span></li>
                <li><span>Pixel Density:</span> <span className="text-dark">{item.pixelDensity}</span></li>
                <li><span>Screen Ratio:</span> <span className="text-dark">{item.screenRatio}</span></li>
                <li><span>Introduction Time:</span> <span className="text-dark">{item.introductionTime}</span></li>
                <li><span>Aspect Ratio:</span> <span className="text-dark">{item.aspectRatio}</span></li>
                <li><span>Protection:</span> <span className="text-dark">{item.protection}</span></li>
                <li><span>Chip:</span> <span className="text-dark">{item.chip}</span></li>
                <li><span>Central Processor:</span> <span className="text-dark">{item.centralProcessor}</span></li>
                <li><span>Processor Type:</span> <span className="text-dark">{item.processorType}</span></li>
                <li><span>GPU:</span> <span className="text-dark">{item.gpu}</span></li>
                <li><span>2G Network:</span> <span className="text-dark">{item.twoGNetwork}</span></li>
                <li><span>3G Network:</span> <span className="text-dark">{item.threeGNetwork}</span></li>
                <li><span>4G Network:</span> <span className="text-dark">{item.fourGNetwork}</span></li>
                <li><span>5G Network:</span> <span className="text-dark">{item.fiveGNetwork}</span></li>
                <li><span>Wifi:</span> <span className="text-dark">{item.wifi}</span></li>
                <li><span>Bluetooth:</span> <span className="text-dark">{item.bluetooth}</span></li>
                <li><span>Bluetooth Version:</span> <span className="text-dark">{item.bluetoothVersion}</span></li>
                <li><span>RAM:</span> <span className="text-dark">{item.ram}</span></li>
                <li><span>Memory Card Support:</span> <span className="text-dark">{item.memoryCardSupport}</span></li>
                <li><span>Communication Technologies:</span> <span className="text-dark">{item.communicationTechnologies}</span></li>
                <li><span>Radio:</span> <span className="text-dark">{item.radio}</span></li>
                <li><span>Mobile Technology:</span> <span className="text-dark">{item.mobileTechnology}</span></li>
                <li><span>Communication Ports:</span> <span className="text-dark">{item.communicationPorts}</span></li>
                <li><span>Back Camera:</span> <span className="text-dark">{item.backCamera}</span></li>
                <li><span>Photo Resolution:</span> <span className="text-dark">{item.photoResolution}</span></li>
                <li><span>Focus Technology:</span> <span className="text-dark">{item.focusTechnology}</span></li>
                <li><span>Flash:</span> <span className="text-dark">{item.flash}</span></li>
                <li><span>Sound Output:</span> <span className="text-dark">{item.soundOutput}</span></li>
                <li><span>Additional Sound Descriptions:</span> <span className="text-dark">{item.additionalSoundDescriptions}</span></li>
                <li><span>Os:</span> <span className="text-dark">{item.os}</span></li>
                <li><span>Os Version:</span> <span className="text-dark">{item.osVersion}</span></li>
                <li><span>Memory Card:</span> <span className="text-dark">{item.memoryCard}</span></li>
                <li><span>Mobile Locations:</span> <span className="text-dark">{item.mobileLocations}</span></li>
            </ul>
        )
    }
    if (item.category === 'console') {
        return (
            <ul className="features">
                <li><span>Appearance Features:</span> <span className="text-dark">{item.appearanceFeatures}</span></li>
                <li><span>Number of Controller:</span> <span className="text-dark">{item.numberOfController}</span></li>
                <li><span>Communication Technologies:</span> <span className="text-dark">{item.communicationTechnologies}</span></li>
                <li><span>Dimensions:</span> <span className="text-dark">{item.dimensions}</span></li>
                <li><span>Types of Memory:</span> <span className="text-dark">{item.typesOfMemory}</span></li>
                <li><span>Hard Disk Capacity:</span> <span className="text-dark">{item.hardDiskCapacity}</span></li>
                <li><span>Features and Capabilities:</span> <span className="text-dark">{item.featuresAndCapabilities}</span></li>
                <li><span>Sound Output:</span> <span className="text-dark">{item.soundOutput}</span></li>
                <li><span>Video Output:</span> <span className="text-dark">{item.videoOutput}</span></li>
                <li><span>Other Details:</span> <span className="text-dark">{item.otherDetails}</span></li>
                <li><span>RAM:</span> <span className="text-dark">{item.ram}</span></li>
                <li><span>GPU:</span> <span className="text-dark">{item.gpu}</span></li>
                <li><span>CPU:</span> <span className="text-dark">{item.cpu}</span></li>
                <li><span>Drive Type:</span> <span className="text-dark">{item.driveType}</span></li>
                <li><span>Items Included:</span> <span className="text-dark">{item.itemsIncluded}</span></li>
                <li><span>Weight:</span> <span className="text-dark">{item.weight}g</span></li>
            </ul>
        )
    }
}
