export function setFeatures(item) {
    if (item.category === 'laptop') {
        return (
            <p className="info-text mt-2">
                <span>Processor Series:</span> <span className='text-dark fw-bold'>{item.processorSeries}</span><br />
                <span>RAM Memory Capacity:</span> <span className='text-dark fw-bold'>{item.ramMemoryCapacity}</span><br />
                <span>Type of RAM Memory:</span> <span className='text-dark fw-bold'>{item.typeOfRamMemory}</span><br />
                <span>GPU Manufacturer:</span> <span className='text-dark fw-bold'>{item.gpuManufacturer}</span><br />
                <span>Screen Size:</span> <span className='text-dark fw-bold'>{item.screenSize}</span><br />
                <span>Screen Accuracy:</span> <span className='text-dark fw-bold'>{item.screenAccuracy}</span><br />
                <span>Classification:</span> <span className='text-dark fw-bold'>{item.classification}</span>
            </p>
        )
    }
    if (item.category === 'mobile') {
        return (
            <p className="info-text mt-2">
                <span>Screen technology:</span> <span className='text-dark fw-bold'>{item.screenTechnology}</span><br />
                <span>Size:</span> <span className='text-dark fw-bold'>{item.size}</span><br />
                <span>Communication Networks:</span> <span className='text-dark fw-bold'>{item.communicationNetworks}</span><br />
                <span>Internal Memory:</span> <span className='text-dark fw-bold'>{item.internalMemory}</span><br />
                <span>Os Version:</span> <span className='text-dark fw-bold'>{item.operatingSystemVersion}</span><br />
            </p>
        )
    }
    if (item.category === 'console') {
        return (
            <p className="info-text mt-2">
                <span>Appearance Features:</span> <span className='text-dark fw-bold'>{item.appearanceFeatures}</span><br />
                <span>Number of Controller:</span> <span className='text-dark fw-bold'>{item.numberOfController}</span><br />
                <span>Communication technologies:</span> <span className='text-dark fw-bold'>{item.communicationTechnologies}</span><br />
            </p>
        )
    }
}
