import React from 'react'
import FeaturedCategories from '../../Components/Shop/HomePage/FeaturedCategories'
import HeroArea from '../../Components/Shop/HomePage/HeroArea'
import TrendingProduct from '../../Components/Shop/HomePage/TrendingProduct/'
import Banner from '../../Components/Shop/HomePage/Banner/'
import SpecialOffer from '../../Components/Shop/HomePage/SpecialOffer/'
import TopProducts from '../../Components/Shop/HomePage/TopProducts/'
import Blog from '../../Components/Shop/HomePage/Blog/'
import ShippingInfo from '../../Components/Shop/HomePage/ShippingInfo/'
import Layout from '../../Components/Shop/Layout/Layout'
import { trendProducts } from './trendProducts'

export default function HomePage({cartItems}) {

    const trends = trendProducts()
    return (
        <>
            <Layout pgTitle={'Home'} cartItems={cartItems}>
                <HeroArea />
                <FeaturedCategories />
                <TrendingProduct title='Latest Laptop' data={trends.laptop} />
                <TrendingProduct title='Latest Mobile' data={trends.mobile} />
                <TrendingProduct title='Latest Console' data={trends.console} />
                <Banner />
                <SpecialOffer />
                <TopProducts />
                <Blog />
                <ShippingInfo />
            </Layout>
        </>
    )
}

