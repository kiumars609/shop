import { ProductsApi } from "../../APIs/ProductsApi"
import { GetAPI } from "../../CustomHooks/GetAPI"

export function trendProducts() {
    const api = ProductsApi()
    const laptop = GetAPI({ url: api.HPLaptop, defaltValue: [] })
    const mobile = GetAPI({ url: api.HPMobile, defaltValue: [] })
    const console = GetAPI({ url: api.HPConsole, defaltValue: [] })
    return { laptop, mobile, console }
}
