import React from 'react'
import { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import Loading from '../../Components/Loading'
import Layout from '../../Components/Shop/Layout/Layout'
import Backend from './Backend'
import ShopLeftSide from './ShopLeftSide/ShopLeftSide'
import { BackendGrid } from './ShopRightSide/BackendGrid'
import { BackendList } from './ShopRightSide/BackendList'
import ShopRightSide from './ShopRightSide/ShopRightSide'
import './style.css'

export default function Shop({ cartItems }) {
    const item = useParams()
    const back = Backend(item)
    const [grid, setGrid] = useState('')
    const [list, setList] = useState('')
    const [sort, setSort] = useState('')
    const [filterBrand, setFilterBrand] = useState([])
    const [filterPrice, setFilterPrice] = useState([])

    // Handle Click When Use Filter Brands
    const handleBrandFilter = (e) => {
        // When CheckBox Clicked
        if (e.target.checked) {
            setFilterBrand([...filterBrand, e.target.value])
        }
        // When CheckBox Not Clicked
        else {
            // Hier First Find Index of Value That Clicked To Disable
            const index = filterBrand.findIndex(obj => obj === e.target.value);
            // After Find Index of Value, Delete That Value Without Another Filter Clicked
            const newData = [
                ...filterBrand.slice(0, index),
                ...filterBrand.slice(index + 1)
            ]
            setFilterBrand(newData)
        }
    }

    // Handle Select Special Optopn (Newes/LowToHigh/HighToLow)
    const handleSort = (e) => {
        setSort(e.target.value)
    }

    // Handle Price Range (Get Min And Max Price)
    const handlePriceRange = (min, max) => {
        const minPrice = min
        const maxPrice = max
        setFilterPrice([minPrice, maxPrice])
    }

    useEffect(() => {
        // When We Have Data, Show Data Map
        if (back.data !== undefined) {
            setGrid(BackendGrid(back.data, sort, filterBrand, filterPrice))
            setList(BackendList(back.data, sort, filterBrand, filterPrice))
        }
        // When We Have No Data
        else {
            setGrid('')
            setList('')
        }
    }, [back.data, sort, filterBrand, filterPrice]);
    return (
        <>
            <Layout cartItems={cartItems} pgTitle='Shop'>
                {back.loader ? <Loading /> :
                    <>
                        <section className="product-grids section">
                            <div className="container">
                                <div className="row">
                                    {/* Left Side (SideBar) */}
                                    <ShopLeftSide handleBrandFilter={handleBrandFilter} handlePriceRange={handlePriceRange} data={back.data} />
                                    {/* Right Side (Products And Sort) */}
                                    <ShopRightSide data={back.data} grid={grid} list={list} sort={sort} handleSort={handleSort} />
                                </div>
                            </div>
                        </section>
                    </>
                }
            </Layout>
        </>
    )
}
