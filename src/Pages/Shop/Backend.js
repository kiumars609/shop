import { useState, useEffect } from 'react';
import { get } from '../../services';

export default function Backend(item) {
    const [data, setData] = useState([])
    const [loader, setLoader] = useState(true);

    useEffect(() => {
        if (item.category === 'laptops-accessories'
            || item.category === 'mobiles-accessories'
            || item.category === 'smart-watches'
            || item.category === 'consoles-accessories'
            || item.category === 'headphones'
            || item.category === 'microphones'
            || item.category === 'televisions'
            || item.category === 'batteries'
            || item.category === 'digital-cameras'
            || item.category === 'cables-adapters'
            || item.category === 'woman'
            || item.category === 'men'
            || item.category === 'kids'
            || item.category === 'beauty-health'
            || item.category === 'home-kitchen'
            || item.category === 'coffee-espresso-machines'
            || item.category === 'home-decore'
            || item.category === 'drugstore'
            || item.category === 'babycare'
            || item.category === 'books'
        ) {
            get(`/products/unknown/unknownAPI.php`)
                .then(response => {
                    setData(response)
                    setLoader(false)
                })
                .catch(function (error) {
                    console.log(error);
                })
                .then(function () {
                    // always executed
                });
        }
        else {
            get(`/products/${item.category}/${item.category}API.php`)
                .then(response => {
                    setData(response)
                    setLoader(false)
                })
                .catch(function (error) {
                    console.log(error);
                })
                .then(function () {
                    // always executed
                });
        }
    }, [item]);

    return { data, loader }
}
