import React from 'react'
import { Link } from 'react-router-dom'

export function BackendList(data, sort, filterBrand,filterPrice) {
    // Use This Function For Set Comma For Prices
    function comma(value) {
        return value.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
    }

    // Get Min And Max Prices For Use In Filter
    const minPrice = filterPrice[0]
    const maxPrice = filterPrice[1]

    // Show Data When Sort Is Null Or Refresh Page (Newest)
    if (sort === '') {
        const mapDataList = data &&
            // This Condition For Use Filter Brand (if)
            filterBrand.length === 0 ?
            data.filter(item => item.price >= parseInt(minPrice) && item.price <= parseInt(maxPrice)).map(item => {
                return (
                    <div key={item.id} className="col-lg-12 col-md-12 col-12">
                        <div className="single-product">
                            <div className="row align-items-center">
                                <div className="col-lg-4 col-md-4 col-12">
                                    <div className="product-image">
                                        <img src={`/assets/main/images/products/${item.category}/${item.img1}`} alt={item.titleSmall} />
                                        <div className="button">
                                            <Link className="btn" to={`/product/${item.category}/${item.id}`}><i className="lni lni-cart"></i> More Info</Link>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-8 col-md-8 col-12">
                                    <div className="product-info">
                                        <Link className="category" to={`/shop/${item.category}`}>{item.category}</Link>
                                        <h4 className="title">
                                            <Link to={`/product/${item.category}/${item.id}`}>{item.title}</Link>
                                        </h4>
                                        <ul className="review">
                                            <li><i className="lni lni-star-filled"></i></li>
                                            <li><i className="lni lni-star-filled"></i></li>
                                            <li><i className="lni lni-star-filled"></i></li>
                                            <li><i className="lni lni-star-filled"></i></li>
                                            <li><i className="lni lni-star"></i></li>
                                            <li><span>4.0 Review(s)</span></li>
                                        </ul>
                                        <div className="price">
                                            <span>€{comma(item.price)}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                )
            })
            // This Condition For Use Filter Brand (else)
            :
            data.filter(filter => filterBrand.includes(filter.brand)).filter(item => item.price >= parseInt(minPrice) && item.price <= parseInt(maxPrice)).map(item => {
                return (
                    <div key={item.id} className="col-lg-12 col-md-12 col-12">
                        <div className="single-product">
                            <div className="row align-items-center">
                                <div className="col-lg-4 col-md-4 col-12">
                                    <div className="product-image">
                                        <img src={`/assets/main/images/products/${item.category}/${item.img1}`} alt={item.titleSmall} />
                                        <div className="button">
                                            <Link className="btn" to={`/product/${item.category}/${item.id}`}><i className="lni lni-cart"></i> More Info</Link>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-8 col-md-8 col-12">
                                    <div className="product-info">
                                        <Link className="category" to={`/shop/${item.category}`}>{item.category}</Link>
                                        <h4 className="title">
                                            <Link to={`/product/${item.category}/${item.id}`}>{item.title}</Link>
                                        </h4>
                                        <ul className="review">
                                            <li><i className="lni lni-star-filled"></i></li>
                                            <li><i className="lni lni-star-filled"></i></li>
                                            <li><i className="lni lni-star-filled"></i></li>
                                            <li><i className="lni lni-star-filled"></i></li>
                                            <li><i className="lni lni-star"></i></li>
                                            <li><span>4.0 Review(s)</span></li>
                                        </ul>
                                        <div className="price">
                                            <span>€{comma(item.price)}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                )
            })
        return mapDataList
    }
    // Show Data When Sort Is Low To High
    if (sort === 'LowToHigh') {
        const mapDataList = data &&
            // This Condition For Use Sort (if)
            filterBrand.length === 0 ?
            data.sort((a, b) => parseInt(a.price) > parseInt(b.price) ? 1 : -1).filter(item => item.price >= parseInt(minPrice) && item.price <= parseInt(maxPrice)).map(item => {
                return (
                    <div key={item.id} className="col-lg-12 col-md-12 col-12">
                        <div className="single-product">
                            <div className="row align-items-center">
                                <div className="col-lg-4 col-md-4 col-12">
                                    <div className="product-image">
                                        <img src={`/assets/main/images/products/${item.category}/${item.img1}`} alt={item.titleSmall} />
                                        <div className="button">
                                            <Link className="btn" to={`/product/${item.category}/${item.id}`}><i className="lni lni-cart"></i> More Info</Link>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-8 col-md-8 col-12">
                                    <div className="product-info">
                                        <Link className="category" to={`/shop/${item.category}`}>{item.category}</Link>
                                        <h4 className="title">
                                            <Link to={`/product/${item.category}/${item.id}`}>{item.title}</Link>
                                        </h4>
                                        <ul className="review">
                                            <li><i className="lni lni-star-filled"></i></li>
                                            <li><i className="lni lni-star-filled"></i></li>
                                            <li><i className="lni lni-star-filled"></i></li>
                                            <li><i className="lni lni-star-filled"></i></li>
                                            <li><i className="lni lni-star"></i></li>
                                            <li><span>4.0 Review(s)</span></li>
                                        </ul>
                                        <div className="price">
                                            <span>€{comma(item.price)}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                )
            })
            // This Condition For Use Sort And Filter Brand (else)
            :
            data.sort((a, b) => parseInt(a.price) > parseInt(b.price) ? 1 : -1).filter(filter => filterBrand.includes(filter.brand)).filter(item => item.price >= parseInt(minPrice) && item.price <= parseInt(maxPrice)).map(item => {
                return (
                    <div key={item.id} className="col-lg-12 col-md-12 col-12">
                        <div className="single-product">
                            <div className="row align-items-center">
                                <div className="col-lg-4 col-md-4 col-12">
                                    <div className="product-image">
                                        <img src={`/assets/main/images/products/${item.category}/${item.img1}`} alt={item.titleSmall} />
                                        <div className="button">
                                            <Link className="btn" to={`/product/${item.category}/${item.id}`}><i className="lni lni-cart"></i> More Info</Link>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-8 col-md-8 col-12">
                                    <div className="product-info">
                                        <Link className="category" to={`/shop/${item.category}`}>{item.category}</Link>
                                        <h4 className="title">
                                            <Link to={`/product/${item.category}/${item.id}`}>{item.title}</Link>
                                        </h4>
                                        <ul className="review">
                                            <li><i className="lni lni-star-filled"></i></li>
                                            <li><i className="lni lni-star-filled"></i></li>
                                            <li><i className="lni lni-star-filled"></i></li>
                                            <li><i className="lni lni-star-filled"></i></li>
                                            <li><i className="lni lni-star"></i></li>
                                            <li><span>4.0 Review(s)</span></li>
                                        </ul>
                                        <div className="price">
                                            <span>€{comma(item.price)}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                )
            })
        return mapDataList
    }
    // Show Data When Sort Is High To Low
    if (sort === 'HighToLow') {
        const mapDataList = data &&
            // This Condition For Use Sort (if)
            filterBrand.length === 0 ?
            data.sort((a, b) => parseInt(a.price) > parseInt(b.price) ? -1 : 1).filter(item => item.price >= parseInt(minPrice) && item.price <= parseInt(maxPrice)).map(item => {
                return (
                    <div key={item.id} className="col-lg-12 col-md-12 col-12">
                        <div className="single-product">
                            <div className="row align-items-center">
                                <div className="col-lg-4 col-md-4 col-12">
                                    <div className="product-image">
                                        <img src={`/assets/main/images/products/${item.category}/${item.img1}`} alt={item.titleSmall} />
                                        <div className="button">
                                            <Link className="btn" to={`/product/${item.category}/${item.id}`}><i className="lni lni-cart"></i> More Info</Link>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-8 col-md-8 col-12">
                                    <div className="product-info">
                                        <Link className="category" to={`/shop/${item.category}`}>{item.category}</Link>
                                        <h4 className="title">
                                            <Link to={`/product/${item.category}/${item.id}`}>{item.title}</Link>
                                        </h4>
                                        <ul className="review">
                                            <li><i className="lni lni-star-filled"></i></li>
                                            <li><i className="lni lni-star-filled"></i></li>
                                            <li><i className="lni lni-star-filled"></i></li>
                                            <li><i className="lni lni-star-filled"></i></li>
                                            <li><i className="lni lni-star"></i></li>
                                            <li><span>4.0 Review(s)</span></li>
                                        </ul>
                                        <div className="price">
                                            <span>€{comma(item.price)}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                )
            })
            // This Condition For Use Sort And Filter Brand (if)
            :
            data.sort((a, b) => parseInt(a.price) > parseInt(b.price) ? -1 : 1).filter(filter => filterBrand.includes(filter.brand)).filter(item => item.price >= parseInt(minPrice) && item.price <= parseInt(maxPrice)).map(item => {
                return (
                    <div key={item.id} className="col-lg-12 col-md-12 col-12">
                        <div className="single-product">
                            <div className="row align-items-center">
                                <div className="col-lg-4 col-md-4 col-12">
                                    <div className="product-image">
                                        <img src={`/assets/main/images/products/${item.category}/${item.img1}`} alt={item.titleSmall} />
                                        <div className="button">
                                            <Link className="btn" to={`/product/${item.category}/${item.id}`}><i className="lni lni-cart"></i> More Info</Link>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-8 col-md-8 col-12">
                                    <div className="product-info">
                                        <Link className="category" to={`/shop/${item.category}`}>{item.category}</Link>
                                        <h4 className="title">
                                            <Link to={`/product/${item.category}/${item.id}`}>{item.title}</Link>
                                        </h4>
                                        <ul className="review">
                                            <li><i className="lni lni-star-filled"></i></li>
                                            <li><i className="lni lni-star-filled"></i></li>
                                            <li><i className="lni lni-star-filled"></i></li>
                                            <li><i className="lni lni-star-filled"></i></li>
                                            <li><i className="lni lni-star"></i></li>
                                            <li><span>4.0 Review(s)</span></li>
                                        </ul>
                                        <div className="price">
                                            <span>€{comma(item.price)}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                )
            })
        return mapDataList
    }
}