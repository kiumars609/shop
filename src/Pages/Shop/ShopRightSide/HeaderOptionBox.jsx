import React from 'react'

export default function HeaderOptionBox({ handleSort }) {
    return (
        <>
            <div className="product-grid-topbar">
                <div className="row align-items-center">
                    <div className="col-lg-7 col-md-8 col-12">
                        <div className="product-sorting">
                            <label htmlFor="sorting">Sort by:</label>
                            <select className="form-control" id="sorting" onChange={(e) => handleSort(e)}>
                                <option value=''>Newest</option>
                                <option value='LowToHigh'>Low - High Price</option>
                                <option value='HighToLow'>High - Low Price</option>
                            </select>
                            <h3 className="total-show-product">Showing: <span>1 - 12 items</span></h3>
                        </div>
                    </div>
                    <div className="col-lg-5 col-md-4 col-12">
                        <nav>
                            <div className="nav nav-tabs" id="nav-tab" role="tablist">
                                <button className="nav-link active" id="nav-grid-tab" data-bs-toggle="tab" data-bs-target="#nav-grid" type="button" role="tab" aria-controls="nav-grid" aria-selected="true"><i className="lni lni-grid-alt"></i></button>
                                <button className="nav-link" id="nav-list-tab" data-bs-toggle="tab" data-bs-target="#nav-list" type="button" role="tab" aria-controls="nav-list" aria-selected="false"><i className="lni lni-list"></i></button>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </>
    )
}
