import React from 'react'
import HeaderOptionBox from './HeaderOptionBox'

export default function ShopRightSide({ data, grid, list, sort, handleSort }) {
    return (
        <>
            <div className={`${data === undefined ? 'col-lg-12' : 'col-lg-9'} col-12`}>
                <div className="product-grids-head">
                    {data === undefined ?
                        <div className="col-md-12 text-center">
                            <img src='/assets/main/images/not-found.png' alt='not-found' />
                        </div>
                        :
                        <>
                            {/* Sort Component */}
                            <HeaderOptionBox sort={sort} handleSort={handleSort} />
                            <div className="tab-content" id="nav-tabContent">
                                {/* ------------------------- Grid ------------------------------ */}
                                <div className="tab-pane fade show active" id="nav-grid" role="tabpanel" aria-labelledby="nav-grid-tab">
                                    <div className="row">
                                        {grid}
                                    </div>
                                    <div className="row">
                                        <div className="col-12">
                                            <div className="pagination left">
                                                {/* <ul className="pagination-list">
                                            <li><a href="javascript:void(0)">1</a></li>
                                            <li className="active"><a href="javascript:void(0)">2</a></li>
                                            <li><a href="javascript:void(0)">3</a></li>
                                            <li><a href="javascript:void(0)">4</a></li>
                                            <li><a href="javascript:void(0)"><i className="lni lni-chevron-right"></i></a></li>
                                        </ul> */}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {/* ------------------------- List ------------------------------ */}
                                <div className="tab-pane fade" id="nav-list" role="tabpanel" aria-labelledby="nav-list-tab">
                                    <div className="row">
                                        {list}
                                    </div>
                                    <div className="row">
                                        <div className="col-12">
                                            <div className="pagination left">
                                                <ul className="pagination-list">
                                                    {/* <li><a href="javascript:void(0)">1</a></li>
                                            <li className="active"><a href="javascript:void(0)">2</a></li>
                                            <li><a href="javascript:void(0)">3</a></li>
                                            <li><a href="javascript:void(0)">4</a></li>
                                            <li><a href="javascript:void(0)"><i className="lni lni-chevron-right"></i></a></li> */}
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </>
                    }


                </div>
            </div>
        </>
    )
}
