import React from 'react'
import FilterBrandBox from './FilterBrand/FilterBrandBox'
import FilterPriceBox from './FilterPriceBox'
// import CategoryBox from './CategoryBox'
// import PriceRangeBox from './PriceRangeBox'
// import SearchBox from './SearchBox'

export default function ShopLeftSide({ data, handleBrandFilter, handlePriceRange }) {
    return (
        <>
            {data !== undefined &&
                <div className="col-lg-3 col-12">
                    <div className="product-sidebar">
                        <FilterBrandBox handleBrandFilter={handleBrandFilter} data={data} />
                        <FilterPriceBox handlePriceRange={handlePriceRange} data={data} />
                        {/* <PriceRangeBox /> */}
                        {/* <SearchBox /> */}
                        {/* <CategoryBox /> */}
                    </div>
                </div>
            }
        </>
    )
}
