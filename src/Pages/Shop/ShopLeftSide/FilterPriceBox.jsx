import React from 'react'
import { useState, useEffect } from 'react'
import '../style.css'

export default function FilterPriceBox({ handlePriceRange, data }) {
    var maxPrice = Math.max(...data.map(item => item.price));
    const [min, setMin] = useState(0)
    const [max, setMax] = useState(maxPrice + 1)
    useEffect(() => {
        handlePriceRange(min, max)
    }, [min, max]);

    return (
        <>
            <div className="single-widget condition">
                <h3>Filter by Price</h3>
                <form>
                    <div className="row">
                        <div className="col-md-1 p-0 title-price position-relative">
                            From
                            <br />
                            <span className='currency-filter-price'>€</span>
                        </div>
                        <div className="col-md-11">
                            <input type='number' value={min} className='form-control price-range mb-3' onChange={(e) => setMin(e.target.value)} />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-1 p-0 title-price position-relative">
                            To
                            <br />
                            <span className='currency-filter-price'>€</span>
                        </div>
                        <div className="col-md-11">
                            <input type='number' value={max} className='form-control price-range mb-3' onChange={(e) => setMax(e.target.value)} />
                        </div>
                    </div>
                </form>
            </div>
        </>
    )
}
