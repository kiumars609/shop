import React from 'react'
import FilterBrandBoxSingle from './FilterBrandBoxSingle'

export default function FilterBrandBox({ handleBrandFilter, data }) {
    return (
        <>
            <div className="single-widget condition">
                <h3>Filter by Brand</h3>
                <FilterBrandBoxSingle data={data} handleBrandFilter={handleBrandFilter} category={data[0]['category']} />
            </div>
        </>
    )
}
