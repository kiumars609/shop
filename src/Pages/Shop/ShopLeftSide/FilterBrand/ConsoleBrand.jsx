import React from 'react'

export default function ConsoleBrand({ handleBrandFilter }) {
    return (
        <>
            <div className="form-check">
                <input className="form-check-input" type="checkbox" value="sony" id="checkConsoleSony" onChange={(e) => handleBrandFilter(e)} />
                <label className="form-check-label" htmlFor="checkConsoleSony">
                    Sony
                </label>
            </div>
            <div className="form-check">
                <input className="form-check-input" type="checkbox" value="microsoft" id="checkConsoleMicrosoft" onChange={(e) => handleBrandFilter(e)} />
                <label className="form-check-label" htmlFor="checkConsoleMicrosoft">
                    Microsoft
                </label>
            </div>
            <div className="form-check">
                <input className="form-check-input" type="checkbox" value="nintendo" id="checkConsoleNintendo" onChange={(e) => handleBrandFilter(e)} />
                <label className="form-check-label" htmlFor="checkConsoleNintendo">
                    Nintendo
                </label>
            </div>
        </>
    )
}