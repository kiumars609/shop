import React from 'react'

export default function LaptopBrand({ handleBrandFilter }) {
    return (
        <>
            <div className="form-check">
                <input className="form-check-input" type="checkbox" value="apple" id="checkLaptopApple" onChange={(e) => handleBrandFilter(e)} />
                <label className="form-check-label" htmlFor="checkLaptopApple">
                    Apple
                </label>
            </div>
            <div className="form-check">
                <input className="form-check-input" type="checkbox" value="asus" id="checkLaptopAsus" onChange={(e) => handleBrandFilter(e)} />
                <label className="form-check-label" htmlFor="checkLaptopAsus">
                    Asus
                </label>
            </div>
            <div className="form-check">
                <input className="form-check-input" type="checkbox" value="lenovo" id="checkLaptopLenovo" onChange={(e) => handleBrandFilter(e)} />
                <label className="form-check-label" htmlFor="checkLaptopLenovo">
                    Lenovo
                </label>
            </div>
            <div className="form-check">
                <input className="form-check-input" type="checkbox" value="hp" id="checkLaptopHP" onChange={(e) => handleBrandFilter(e)} />
                <label className="form-check-label" htmlFor="checkLaptopHP">
                    HP
                </label>
            </div>
            <div className="form-check">
                <input className="form-check-input" type="checkbox" value="acer" id="checkLaptopAcer" onChange={(e) => handleBrandFilter(e)} />
                <label className="form-check-label" htmlFor="checkLaptopAcer">
                    Acer
                </label>
            </div>
            <div className="form-check">
                <input className="form-check-input" type="checkbox" value="msi" id="checkLaptopMSI" onChange={(e) => handleBrandFilter(e)} />
                <label className="form-check-label" htmlFor="checkLaptopMSI">
                    MSI
                </label>
            </div>
            <div className="form-check">
                <input className="form-check-input" type="checkbox" value="dell" id="checkLaptopDell" onChange={(e) => handleBrandFilter(e)} />
                <label className="form-check-label" htmlFor="checkLaptopDell">
                    Dell
                </label>
            </div>
            <div className="form-check">
                <input className="form-check-input" type="checkbox" value="microsoft" id="checkLaptopMicrosoft" onChange={(e) => handleBrandFilter(e)} />
                <label className="form-check-label" htmlFor="checkLaptopMicrosoft">
                    Microsoft
                </label>
            </div>
            <div className="form-check">
                <input className="form-check-input" type="checkbox" value="huawei" id="checkLaptopHuawei" onChange={(e) => handleBrandFilter(e)} />
                <label className="form-check-label" htmlFor="checkLaptopHuawei">
                    Huawei
                </label>
            </div>
            <div className="form-check">
                <input className="form-check-input" type="checkbox" value="razer" id="checkLaptopRazer" onChange={(e) => handleBrandFilter(e)} />
                <label className="form-check-label" htmlFor="checkLaptopRazer">
                    Razer
                </label>
            </div>
            <div className="form-check">
                <input className="form-check-input" type="checkbox" value="samsung" id="checkLaptopSamsung" onChange={(e) => handleBrandFilter(e)} />
                <label className="form-check-label" htmlFor="checkLaptopSamsung">
                    Samsung
                </label>
            </div>
        </>
    )
}
