import React from 'react'

export default function MobileBrand({ handleBrandFilter }) {
    return (
        <>
            <div className="form-check">
                <input className="form-check-input" type="checkbox" value="apple" id="checkMobileApple" onChange={(e) => handleBrandFilter(e)} />
                <label className="form-check-label" htmlFor="checkMobileApple">
                    Apple
                </label>
            </div>
            <div className="form-check">
                <input className="form-check-input" type="checkbox" value="samsung" id="checkMobileSamsung" onChange={(e) => handleBrandFilter(e)} />
                <label className="form-check-label" htmlFor="checkMobileSamsung">
                    Samsung
                </label>
            </div>
            <div className="form-check">
                <input className="form-check-input" type="checkbox" value="huawei" id="checkMobileHuawei" onChange={(e) => handleBrandFilter(e)} />
                <label className="form-check-label" htmlFor="checkMobileHuawei">
                    Huawei
                </label>
            </div>
            <div className="form-check">
                <input className="form-check-input" type="checkbox" value="xiaomi" id="checkMobileXiaomi" onChange={(e) => handleBrandFilter(e)} />
                <label className="form-check-label" htmlFor="checkMobileXiaomi">
                    Xiaomi
                </label>
            </div>
            <div className="form-check">
                <input className="form-check-input" type="checkbox" value="nokia" id="checkMobileNokia" onChange={(e) => handleBrandFilter(e)} />
                <label className="form-check-label" htmlFor="checkMobileNokia">
                    Nokia
                </label>
            </div>
            <div className="form-check">
                <input className="form-check-input" type="checkbox" value="honor" id="checkMobileHonor" onChange={(e) => handleBrandFilter(e)} />
                <label className="form-check-label" htmlFor="checkMobileHonor">
                    Honor
                </label>
            </div>
            <div className="form-check">
                <input className="form-check-input" type="checkbox" value="lg" id="checkMobileLg" onChange={(e) => handleBrandFilter(e)} />
                <label className="form-check-label" htmlFor="checkMobileLg">
                    LG
                </label>
            </div>
            <div className="form-check">
                <input className="form-check-input" type="checkbox" value="blackBerry" id="checkMobileBlackberry" onChange={(e) => handleBrandFilter(e)} />
                <label className="form-check-label" htmlFor="checkMobileBlackberry">
                    BlackBerry
                </label>
            </div>
            <div className="form-check">
                <input className="form-check-input" type="checkbox" value="sony" id="checkMobileSony" onChange={(e) => handleBrandFilter(e)} />
                <label className="form-check-label" htmlFor="checkMobileSony">
                    Sony
                </label>
            </div>
            <div className="form-check">
                <input className="form-check-input" type="checkbox" value="asus" id="checkMobileAsus" onChange={(e) => handleBrandFilter(e)} />
                <label className="form-check-label" htmlFor="checkMobileAsus">
                    ASUS
                </label>
            </div>
            <div className="form-check">
                <input className="form-check-input" type="checkbox" value="htc" id="checkMobileHtc" onChange={(e) => handleBrandFilter(e)} />
                <label className="form-check-label" htmlFor="checkMobileHtc">
                    HTC
                </label>
            </div>
        </>
    )
}
