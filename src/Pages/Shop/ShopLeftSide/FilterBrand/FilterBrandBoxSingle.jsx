import React from 'react'
import ConsoleBrand from './ConsoleBrand'
import LaptopBrand from './LaptopBrand'
import MobileBrand from './MobileBrand'

export default function FilterBrandBoxSingle({ handleBrandFilter, category }) {
    return (
        <>
            {'laptop' === category &&
                <LaptopBrand handleBrandFilter={handleBrandFilter} />
            }
            {'console' === category &&
                <ConsoleBrand handleBrandFilter={handleBrandFilter} />
            }
            {'mobile' === category &&
                <MobileBrand handleBrandFilter={handleBrandFilter} />
            }
        </>
    )
}
