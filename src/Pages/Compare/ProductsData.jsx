import React from 'react'
import CaseData from './CaseData'
import ConsoleData from './ConsoleData'
import LaptopData from './LaptopData'
import MobileData from './MobileData'

export default function ProductsData({ category, item }) {
    return (
        <>
            {'laptop' === category &&
                <>
                    <LaptopData item={item} />
                </>
            }
            {'mobile' === category &&
                <>
                    <MobileData item={item} />
                </>
            }
            {'console' === category &&
                <>
                    <ConsoleData item={item} />
                </>
            }
            {'case' === category &&
                <>
                    <CaseData item={item} />
                </>
            }
        </>
    )
}
