import React from 'react'
import CaseData from './CaseData';
import ConsoleData from './ConsoleData';
import LaptopData from './LaptopData'
import MobileData from './MobileData'

export default function ProductsDataSelected({ category, item }) {
    console.log(category + ' - ' + item);
    return (
        <>
            {'laptop' === category &&
                <LaptopData category={category} item={item} />
            }
            {'mobile' === category &&
                <MobileData category={category} item={item} />
            }
            {'console' === category &&
                <ConsoleData category={category} item={item} />
            }
            {'case' === category &&
                <CaseData category={category} item={item} />
            }
        </>
    )
}
