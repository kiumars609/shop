import React from 'react'
import Modal from 'react-bootstrap/Modal';
import Form from 'react-bootstrap/Form';
import ProductSingle from './ProductSingle';
import './style.css'
import { useState, useEffect } from 'react';
import { get } from '../../../services';
import useDebounce from '../../../CustomHooks/useDebounce';

export default function SearchModal({ handleCloseModal, showModal, categorySearch, handleSelectProductSecond, handleSelectProductThird, rowproductSelected, filterProductFirst, filterProductSecond }) {

    const [data, setData] = useState([])
    const [search, setSearch] = useState('')
    const [filterSearch, setFilterSearch] = useState([])
    const [showSearch, setShowSearch] = useState(false);
    const debouncedSearchTerm = useDebounce(search, 1500)
    const handleCloseSearch = () => setShowSearch(false);
    const handleShowSearch = () => setShowSearch(true);

    // Get Data For Serach Product
    useEffect(() => {
        get('/products/searchCompare/searchCompareAPI.php')
            .then(response => {
                setData(response)
            })
    }, []);

    // Search
    useEffect(() => {
        setFilterSearch(data &&
            data.filter(results => {
                return results.title.toLowerCase().includes(debouncedSearchTerm.toLowerCase())
            })
        )
        if (search !== '') {
            handleShowSearch()
        }
        else {
            handleCloseSearch()
        }
    }, [debouncedSearchTerm, data, search]);

    // Map And Filter Search Data
    const mapData = filterSearch && filterSearch.filter(item => item.category === categorySearch).filter(a => a.id !== filterProductFirst & a.id !== filterProductSecond).map((row, index) => {
        return (
            <ProductSingle
                key={index}
                item={row}
                handleSelectProductSecond={handleSelectProductSecond}
                handleSelectProductThird={handleSelectProductThird}
                handleCloseModal={handleCloseModal}
                rowproductSelected={rowproductSelected}
            />
        )
    })
    return (
        <>
            <Modal className='modal modal-card modal-search-compare' show={showModal} centered size="lg" onHide={handleCloseModal}>
                <Modal.Header closeButton>
                    <Modal.Title>Select a product to compare</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                        <Form.Control
                            type="text"
                            placeholder={`Search in ${categorySearch} category`}
                            autoFocus
                            onChange={e => setSearch(e.target.value)}
                        />
                    </Form.Group>
                    <div className="row mx-auto">
                        {mapData}
                    </div>
                </Modal.Body>
            </Modal>
        </>
    )
}
