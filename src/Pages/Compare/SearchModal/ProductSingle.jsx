import React from 'react'
import { useCommaSeparators } from '../../../CustomHooks/useCommaSeparators'

export default function ProductSingle({ item, handleSelectProductSecond, handleSelectProductThird, handleCloseModal, rowproductSelected }) {
    return (
        <>
            <div className="col-lg-6 col-md-6 col-12">
                {/* Onclick For Get Product Id */}
                <div className="single-product" role='button' onClick={() => {
                    'secondProduct' === rowproductSelected && handleSelectProductSecond(item.id); handleSelectProductThird('');
                    'thirdProduct' === rowproductSelected && handleSelectProductThird(item.id);
                    handleCloseModal()
                }}>
                    <div className="product-image">
                        <img src={`/assets/main/images/products/${item.category}/${item.img1}`} alt='' />
                    </div>
                    <div className="product-info">
                        <div className='row mx-auto col-md-12 p-0'>
                            <div className='col-md-12'>
                                <h4 className="title">
                                    <span>{item.titleSmall}</span>
                                </h4>
                                <ul className="review">
                                    <li><i className="lni lni-star-filled"></i></li>
                                    <li><i className="lni lni-star-filled"></i></li>
                                    <li><i className="lni lni-star-filled"></i></li>
                                    <li><i className="lni lni-star-filled"></i></li>
                                    <li><i className="lni lni-star"></i></li>
                                    <li><span>4.0 Review(s)</span></li>
                                </ul>
                                <div className="price">
                                    <span>€{useCommaSeparators(item.price)}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
