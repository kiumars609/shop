import React from 'react'
import { useState, useEffect } from 'react'
import { get } from '../../services'
import ProductsDataSelected from './ProductsDataSelected'

export default function Backend(item) {

    const [data, setData] = useState('')
    const [dataSelected, setDataSelected] = useState([])
    const [productSelectedSecond, setProductSelectedSecond] = useState('')
    const [productSelectedThird, setProductSelectedThird] = useState('')
    const [rowproductSelected, setRowproductSelected] = useState('')
    const [loader, setLoader] = useState(true);
    const [showModal, setShowModal] = useState(false);
    const handleCloseModal = () => setShowModal(false);
    // const handleShowModal = () => setShowModal(true);

    // Get Data For First Product (Main Product)
    useEffect(() => {
        get(`/products/${item.category}/${item.category}API.php`)
            .then(response => {
                for (let index = 0; index < response.length; index++) {
                    if (response[index].id === item.id) {
                        const element = response[index];
                        setData(element)
                        setLoader(false)
                    }
                }
            })
            .catch(function (error) {
                console.log(error);
            })
            .then(function () {
                // always executed
            });
    }, [item]);

    // Handle Get ID For Second Product (After Click and Select Product)
    const handleSelectProductSecond = (value) => {
        setProductSelectedSecond(value)
    }

    // Handle Get ID For Third Product (After Click and Select Product)
    const handleSelectProductThird = (value) => {
        setProductSelectedThird(value)
    }

    // Get Data After Select Second OR Third Products
    useEffect(() => {
        get(`/products/${item.category}/${item.category}API.php`)
            .then(response => {
                setDataSelected(response)
                setLoader(false)
            })
            .catch(function (error) {
                console.log(error);
            })
            .then(function () {
                // always executed
            });
    }, [item.category]);

    // Map And Filter Second Product
    const mapproductSelectedSecond = dataSelected && dataSelected.filter(a => a.id === productSelectedSecond).map(itm => <ProductsDataSelected category={item.category} key={itm.id} item={itm} />)
    // Map And Filter Third Product
    const mapproductSelectedThird = dataSelected && dataSelected.filter(a => a.id === productSelectedThird).map(itm => <ProductsDataSelected category={item.category} key={itm.id} item={itm} />)

    // Handle Click Remove Second Product
    const handleCloseproductSelectedSecond = () => {
        setProductSelectedSecond('')
    }

    // Handle Click Remove Third Product
    const handleCloseproductSelectedThird = () => {
        setProductSelectedThird('')
    }

    // Handle Show Modal With Get Value (secondProduct Or thirdProduct)
    const handleNewShowModal = (value) => {
        setShowModal(true);
        setRowproductSelected(value)
    }

    return {
        data,
        loader,
        productSelectedSecond,
        productSelectedThird,
        mapproductSelectedSecond,
        mapproductSelectedThird,
        rowproductSelected,
        showModal,
        handleNewShowModal,
        handleCloseproductSelectedThird,
        handleCloseproductSelectedSecond,
        handleSelectProductThird,
        handleSelectProductSecond,
        handleCloseModal
    }
}
