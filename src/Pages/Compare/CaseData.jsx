import React from 'react'
import { Link } from 'react-router-dom'
import { useCommaSeparators } from '../../CustomHooks/useCommaSeparators'

export default function CaseData({ item }) {
    return (
        <>
            <div className="col-md-12 text-center">
                <img src={`/assets/main/images/products/${item.category}/${item.img1}`} alt={item.titleSmall} className='col-md-6' />
                <Link to={`/product/${item.category}/${item.id}`} className='title-compare'><h5>{item.title}</h5></Link>
                <p className='mt-4 fs-5'>€{useCommaSeparators(item.price)}</p>
            </div>
            <div className="col-md-12 text-left mt-5">
                <span className='text-dark mb-5'>Specifications</span>
                <div className="col-md-12 box-detail mb-4">
                    <span>Weight</span><br />
                    <span className='text-dark my-3'>{item.weight} Kg</span>
                </div>
            </div>
        </>
    )
}
