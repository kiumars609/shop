import React from 'react'
import { Link } from 'react-router-dom'
import { useCommaSeparators } from '../../CustomHooks/useCommaSeparators'
import './style.css'

export default function LaptopData({ item }) {
    return (
        <>
            <div className="col-md-12 text-center">
                <img src={`/assets/main/images/products/${item.category}/${item.img1}`} alt={item.titleSmall} className='col-md-6' />
                <Link to={`/product/${item.category}/${item.id}`} className='title-compare'><h5>{item.title}</h5></Link>
                <p className='mt-4 fs-5'>€{useCommaSeparators(item.price)}</p>
            </div>
            <div className="col-md-12 text-left mt-5">
                <span className='text-dark mb-5'>Specifications</span>
                <div className="col-md-12 box-detail mb-4">
                    <span>Weight</span><br />
                    <span className='text-dark my-3'>{item.weight} Kg</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Processor Model</span><br />
                    <span className='text-dark my-3'>{item.processorModel}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Processor Speed Range</span><br />
                    <span className='text-dark my-3'>{item.processorSpeedRange}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Cache</span><br />
                    <span className='text-dark my-3'>{item.cache}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Processor Frequency</span><br />
                    <span className='text-dark my-3'>{item.processorFrequency}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Internal Memory Capacity</span><br />
                    <span className='text-dark my-3'>{item.internalMemoryCapacity}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Internal Memory Type</span><br />
                    <span className='text-dark my-3'>{item.internalMemoryType}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Internal Memory Specifications</span><br />
                    <span className='text-dark my-3'>{item.internalMemorySpecifications}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>GPU Model</span><br />
                    <span className='text-dark my-3'>{item.GPUModel}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>GPU Dedicated Memory</span><br />
                    <span className='text-dark my-3'>{item.GPUDedicatedMemory}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Screen Type</span><br />
                    <span className='text-dark my-3'>{item.screenType}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Device Capabilities</span><br />
                    <span className='text-dark my-3'>{item.deviceCapabilities}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Optical Drive</span><br />
                    <span className='text-dark my-3'>{item.opticalDrive}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Touchpad Specifications</span><br />
                    <span className='text-dark my-3'>{item.touchpadSpecifications}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Communication Ports</span><br />
                    <span className='text-dark my-3'>{item.communicationPorts}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Number of USB 3.0 Ports</span><br />
                    <span className='text-dark my-3'>{item.numberOfUSB3Ports}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Battery Description</span><br />
                    <span className='text-dark my-3'>{item.batteryDescription}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Battery Charging</span><br />
                    <span className='text-dark my-3'>{item.batteryCharging}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>OS</span><br />
                    <span className='text-dark my-3'>{item.OS}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Included Items</span><br />
                    <span className='text-dark my-3'>{item.includedItems}</span>
                </div>
            </div>
        </>
    )
}