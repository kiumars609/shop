import React from 'react'
import { Link } from 'react-router-dom'
import { useCommaSeparators } from '../../CustomHooks/useCommaSeparators'

export default function MobileData({ item }) {
    return (
        <>
            <div className="col-md-12 text-center">
                <img src={`/assets/main/images/products/${item.category}/${item.img1}`} alt={item.titleSmall} className='col-md-6' />
                <Link to={`/product/${item.category}/${item.id}`} className='title-compare'><h5>{item.title}</h5></Link>
                <p className='mt-4 fs-5'>€{useCommaSeparators(item.price)}</p>
            </div>
            <div className="col-md-12 text-left mt-5">
                <span className='text-dark mb-5'>Specifications</span>
                <div className="col-md-12 box-detail mb-4">
                    <span>Weight</span><br />
                    <span className='text-dark my-3'>{item.weight} Kg</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Screen Size Range</span><br />
                    <span className='text-dark my-3'>{item.screenSizeRange}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Sim Card</span><br />
                    <span className='text-dark my-3'>{item.simCard}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Sim Card Number</span><br />
                    <span className='text-dark my-3'>{item.simCardNumber}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Resolution</span><br />
                    <span className='text-dark my-3'>{item.resolution}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Pixel Density</span><br />
                    <span className='text-dark my-3'>{item.pixelDensity}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Screen Ratio</span><br />
                    <span className='text-dark my-3'>{item.screenRatio}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Introduction Time</span><br />
                    <span className='text-dark my-3'>{item.introductionTime}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Aspect Ratio</span><br />
                    <span className='text-dark my-3'>{item.aspectRatio}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Protection</span><br />
                    <span className='text-dark my-3'>{item.protection}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Chip</span><br />
                    <span className='text-dark my-3'>{item.chip}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Central Processor</span><br />
                    <span className='text-dark my-3'>{item.centralProcessor}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Processor Type</span><br />
                    <span className='text-dark my-3'>{item.processorType}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>GPU</span><br />
                    <span className='text-dark my-3'>{item.gpu}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>2G Network</span><br />
                    <span className='text-dark my-3'>{item.twoGNetwork}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>3G Network</span><br />
                    <span className='text-dark my-3'>{item.threeGNetwork}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>4G Network</span><br />
                    <span className='text-dark my-3'>{item.fourGNetwork}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>5G Network</span><br />
                    <span className='text-dark my-3'>{item.fiveGNetwork}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Wifi</span><br />
                    <span className='text-dark my-3'>{item.wifi}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Bluetooth</span><br />
                    <span className='text-dark my-3'>{item.bluetooth}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Bluetooth Version</span><br />
                    <span className='text-dark my-3'>{item.bluetoothVersion}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>RAM</span><br />
                    <span className='text-dark my-3'>{item.ram}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Memory Card Support</span><br />
                    <span className='text-dark my-3'>{item.memoryCardSupport}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Communication Technologies</span><br />
                    <span className='text-dark my-3'>{item.communicationTechnologies}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Radio</span><br />
                    <span className='text-dark my-3'>{item.radio}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Mobile Technology</span><br />
                    <span className='text-dark my-3'>{item.mobileTechnology}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Communication Ports</span><br />
                    <span className='text-dark my-3'>{item.communicationPorts}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Back Camera</span><br />
                    <span className='text-dark my-3'>{item.backCamera}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Photo Resolution</span><br />
                    <span className='text-dark my-3'>{item.photoResolution}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Focus Technology</span><br />
                    <span className='text-dark my-3'>{item.focusTechnology}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Flash</span><br />
                    <span className='text-dark my-3'>{item.flash}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Sound Output</span><br />
                    <span className='text-dark my-3'>{item.soundOutput}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Additional Sound Descriptions</span><br />
                    <span className='text-dark my-3'>{item.additionalSoundDescriptions}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Os</span><br />
                    <span className='text-dark my-3'>{item.os}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Os Version</span><br />
                    <span className='text-dark my-3'>{item.osVersion}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Memory Card</span><br />
                    <span className='text-dark my-3'>{item.memoryCard}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Mobile Locations</span><br />
                    <span className='text-dark my-3'>{item.mobileLocations}</span>
                </div>
            </div>
        </>
    )
}
