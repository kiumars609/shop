import React from 'react'
import { useParams } from 'react-router-dom'
import Loading from '../../Components/Loading'
import Layout from '../../Components/Shop/Layout/Layout'
import Backend from './Backend'
import ProductsData from './ProductsData'
import SearchModal from './SearchModal/SearchModal'
import './style.css'

export default function Compare({ cartItems }) {

    const item = useParams();
    const back = Backend(item)
    return (
        <>
            <Layout cartItems={cartItems} pgTitle='Products-Compare'>
                {back.loader ? <Loading /> :
                    <>
                        <div className="row mx-auto p-5">
                            {/* Show First (Main) Product */}
                            <div className={`${'' !== back.productSelectedSecond ? 'col-md-4' : 'col-md-6'}`}>
                                <ProductsData category={back.data.category} item={back.data} />
                            </div>
                            {/* Show Second Product */}
                            <div className={`${'' !== back.productSelectedSecond ? 'col-md-4 position-relative text-left' : 'col-md-6 text-center'}`}>
                                {'' === back.productSelectedSecond ?
                                    <button className='btn btn-warning select-product-btn' onClick={() => back.handleNewShowModal('secondProduct')}>Select Product</button>
                                    :
                                    <>
                                        <span className='cancel-product-selected-btn' role='button' onClick={() => back.handleCloseproductSelectedSecond()}>
                                            <i className="fa-solid fa-circle-xmark"></i>
                                        </span>
                                        {
                                            item.category === back.data.category &&
                                            <>
                                                {back.mapproductSelectedSecond}
                                            </>
                                        }
                                        {/* <ProductsDataSelected category={data.category} item={mapproductSelectedSecond} /> */}
                                    </>
                                }
                            </div>
                            {/* Show Third Product */}
                            {'' !== back.productSelectedSecond && '' === back.productSelectedThird ?
                                <div className={`${'' !== back.productSelectedSecond ? 'col-md-4 text-center' : 'col-md-6'}`}>
                                    <button className='btn btn-warning select-product-btn' onClick={() => back.handleNewShowModal('thirdProduct')}>Select Product</button>
                                </div>
                                :
                                '' !== back.productSelectedSecond && '' !== back.productSelectedThird &&
                                <div className={`${'' !== back.productSelectedThird ? 'col-md-4 position-relative text-left' : 'col-md-6 text-center'}`}>
                                    <>
                                        <span className='cancel-product-selected-btn' role='button' onClick={() => back.handleCloseproductSelectedThird()}>
                                            <i className="fa-solid fa-circle-xmark"></i>
                                        </span>
                                        {
                                            item.category === back.data.category &&
                                            <>
                                                {back.mapproductSelectedThird}
                                            </>
                                        }
                                    </>
                                </div>
                            }
                        </div>
                    </>
                }
                {/* Search Product And Select In Modal */}
                <SearchModal
                    handleCloseModal={back.handleCloseModal}
                    showModal={back.showModal}
                    categorySearch={item.category}
                    handleSelectProductSecond={back.handleSelectProductSecond}
                    handleSelectProductThird={back.handleSelectProductThird}
                    rowproductSelected={back.rowproductSelected}
                    filterProductFirst={item.id}
                    filterProductSecond={back.productSelectedSecond}
                />
            </Layout>
        </>
    )
}
