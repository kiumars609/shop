import React from 'react'
import { Link } from 'react-router-dom'
import { useCommaSeparators } from '../../CustomHooks/useCommaSeparators'

export default function ConsoleData({ item }) {
    return (
        <>
            <div className="col-md-12 text-center">
                <img src={`/assets/main/images/products/${item.category}/${item.img1}`} alt={item.titleSmall} className='col-md-6' />
                <Link to={`/product/${item.category}/${item.id}`} className='title-compare'><h5>{item.title}</h5></Link>
                <p className='mt-4 fs-5'>€{useCommaSeparators(item.price)}</p>
            </div>
            <div className="col-md-12 text-left mt-5">
                <span className='text-dark mb-5'>Specifications</span>
                <div className="col-md-12 box-detail mb-4">
                    <span>Weight</span><br />
                    <span className='text-dark my-3'>{item.weight} Kg</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Appearance Features</span><br />
                    <span className='text-dark my-3'>{item.appearanceFeatures}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Number of Controller</span><br />
                    <span className='text-dark my-3'>{item.numberOfController}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Communication Technologies</span><br />
                    <span className='text-dark my-3'>{item.communicationTechnologies}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Dimensions</span><br />
                    <span className='text-dark my-3'>{item.dimensions}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Types of Memory</span><br />
                    <span className='text-dark my-3'>{item.typesOfMemory}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Hard Disk Capacity</span><br />
                    <span className='text-dark my-3'>{item.hardDiskCapacity}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Features and Capabilities</span><br />
                    <span className='text-dark my-3'>{item.featuresAndCapabilities}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Sound Output</span><br />
                    <span className='text-dark my-3'>{item.soundOutput}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Video Output</span><br />
                    <span className='text-dark my-3'>{item.videoOutput}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Other Details</span><br />
                    <span className='text-dark my-3'>{item.otherDetails}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>RAM</span><br />
                    <span className='text-dark my-3'>{item.ram}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>GPU</span><br />
                    <span className='text-dark my-3'>{item.gpu}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>CPU</span><br />
                    <span className='text-dark my-3'>{item.cpu}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Items Included</span><br />
                    <span className='text-dark my-3'>{item.itemsIncluded}</span>
                </div>
                <div className="col-md-12 box-detail mb-4">
                    <span>Drive Type</span><br />
                    <span className='text-dark my-3'>{item.driveType}</span>
                </div>
            </div>
        </>
    )
}
