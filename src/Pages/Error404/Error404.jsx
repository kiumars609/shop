import React from 'react'
import { Link } from 'react-router-dom'
import './style.css'

export default function Error404() {
    return (
        <>
            <div className="error-area">
                <div className="d-table">
                    <div className="d-table-cell">
                        <div className="container">
                            <div className="error-content">
                                {/* <h1>404</h1>
                                <h2>Oops! Page Not Found!</h2>
                                <p>The page you are looking for does not exist. It might have been moved or deleted.</p> */}
                                <img src='/assets/main/images/404.png' alt='error404' />
                                <div className="button">
                                    <Link to="/" className="btn">Back to Home</Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
