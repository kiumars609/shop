import React from 'react'
import { Link } from 'react-router-dom'

export default function FeaturedPosts({ data }) {

    const mapData = data && data.slice(0, 4).map(item => {
        return (
            <div key={item.id} className="single-popular-feed">
                <div className="feed-desc">
                    <Link to={`/blog/${item.id}`} className="feed-img">
                        <img src={`/assets/main/images/blog/${item.img}`} alt={item.title} />
                    </Link>
                    <h6 className="post-title"><Link to={`/blog/${item.id}`}>{item.title}</Link></h6>
                    <span className="time"><i className="lni lni-calendar"></i> {item.createdAt.slice(0, 10)}</span>
                </div>
            </div>
        )
    })
    return (
        <>
            <div className="widget popular-feeds">
                <h5 className="widget-title">Featured Posts</h5>
                <div className="popular-feed-loop">
                    {mapData}
                </div>
            </div>
        </>
    )
}
