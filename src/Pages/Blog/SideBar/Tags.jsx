import React from 'react'

export default function Tags() {
    return (
        <>
            <div className="widget popular-tag-widget">
                <h5 className="widget-title">Popular Tags</h5>
                <div className="tags">
                    <a href="true">#electronics</a>
                    <a href="true">#cpu</a>
                    <a href="true">#gadgets</a>
                    <a href="true">#wearables</a>
                    <a href="true">#smartphones</a>
                </div>
            </div>
        </>
    )
}
