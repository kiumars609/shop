import React from 'react'
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { get } from '../../../services';
import '../style.css'

export default function Categories() {
    const [data, setData] = useState([])
    useEffect(() => {
        get('/blog/category/categoryAPI.php')
            .then(response => {
                setData(response)
                // setLoader(false)
            })
    }, []);
    const mapData = data && data.map(item => {
        return (
            <li key={item.id}>
                <Link to={`/blog/category/${item.name}`} className='side-categories-title'>{item.name}</Link>
            </li>
        )
    })
    return (
        <>
            <div className="widget categories-widget">
                <h5 className="widget-title">Top Categories</h5>
                <ul className="custom">
                    {mapData}
                </ul>
            </div>
        </>
    )
}
