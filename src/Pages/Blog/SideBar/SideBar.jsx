import React from 'react'
import BlogSearch from './BlogSearch'
import Categories from './Categories'
import FeaturedPosts from './FeaturedPosts'
// import Tags from './Tags'

export default function SideBar({ data }) {
    return (
        <>
            <aside className="col-lg-4 col-md-12 col-12">
                <div className="sidebar blog-grid-page">
                    <BlogSearch />
                    <FeaturedPosts data={data} />
                    <Categories />
                    {/* <Tags /> */}
                </div>
            </aside>
        </>
    )
}
