import React from 'react'
import { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import useDebounce from '../../../CustomHooks/useDebounce'
import { get } from '../../../services'
import '../style.css'

export default function BlogSearch() {

    const [data, setData] = useState([])
    const [search, setSearch] = useState('')
    const [filterSearch, setFilterSearch] = useState([])
    const [showSearch, setShowSearch] = useState(false);
    const debouncedSearchTerm = useDebounce(search, 1500)
    const handleCloseSearch = () => setShowSearch(false);
    const handleShowSearch = () => setShowSearch(true);

    useEffect(() => {
        get('/blog/blogAPI.php')
            .then(response => {
                setData(response)
            })
    }, []);

    useEffect(() => {
        setFilterSearch(
            data.filter(results => {
                return results.title.toLowerCase().includes(debouncedSearchTerm.toLowerCase())
            })
        )
        if (search !== '') {
            handleShowSearch()
        }
        else {
            handleCloseSearch()
        }
    }, [debouncedSearchTerm, data, search]);

    const mapData = filterSearch.map((row, index) => {
        return (
            <div key={index} className={`row mx-auto mb-2 ${index === 0 && 'first-result'}`}>
                <div className="col-md-3">
                    <Link to={`/product/${row.category}/${row.id}`} className='col-md-12 search-results' onClick={handleCloseSearch}>
                        <img src={`/assets/main/images/blog/${row.img}`} alt={row.titleSmall} className='col-md-12' />
                    </Link>
                </div>
                <div className="col-md-9">
                    <Link to={`/product/${row.category}/${row.id}`} className='col-md-12 search-results' onClick={handleCloseSearch}>{row.title}</Link>
                </div>
                <hr className='col-md-11 mx-auto' />
            </div>
        )
    })
    return (
        <>
            <div className="widget search-widget">
                <h5 className="widget-title">Search Your Post</h5>
                <form>
                    <input type="text" placeholder="Search Here..." onChange={e => setSearch(e.target.value)} />
                    <div className={`col-12 main-results-search ${showSearch ? 'd-block' : 'd-none'}`} id='mains'>
                        <i className='fa fa-close mt-2 text-danger' role="button" onClick={handleCloseSearch}></i>
                        {mapData}
                    </div>
                </form>
            </div>
        </>
    )
}
