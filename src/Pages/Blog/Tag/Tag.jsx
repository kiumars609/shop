import React from 'react'
import { useState, useEffect } from 'react';
import Loading from '../../../Components/Loading';
import Layout from '../../../Components/Shop/Layout/Layout'
import { get } from '../../../services';
import SideBar from '../SideBar/SideBar';
import BlogTagSingle from './BlogTagSingle';

export default function Tag({ cartItems }) {

    const [data, setData] = useState([])
    const [loader, setLoader] = useState(true)

    useEffect(() => {
        get('/blog/blogAPI.php')
            .then(response => {
                setData(response)
                setLoader(false)
            })
    }, []);

    return (
        <>
            <Layout cartItems={cartItems} pgTitle='Tag'>
                {loader ? <Loading /> :
                    <section className="section blog-section blog-list">
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-8 col-md-12 col-12">
                                    <div className="row">
                                        <BlogTagSingle data={data} />
                                    </div>
                                    {/* <div className="pagination left blog-grid-page">
                                <ul className="pagination-list">
                                    <li><a href="javascript:void(0)">Prev</a></li>
                                    <li className="active"><a href="javascript:void(0)">2</a></li>
                                    <li><a href="javascript:void(0)">3</a></li>
                                    <li><a href="javascript:void(0)">4</a></li>
                                    <li><a href="javascript:void(0)">Next</a></li>
                                </ul>
                            </div> */}
                                </div>
                                <SideBar data={data} />
                            </div>
                        </div>
                    </section>
                }
            </Layout>
        </>
    )
}
