import React from 'react'
import { Link } from 'react-router-dom'
import './style.css'

export default function BlogSingle({ data }) {

    const mapData = data && data.map(item => {
        // Split Multi Category
        const category = item.category.split(',')
        const newCategory = category.map((i, index) => {
            return (
                <a key={index} className="category blog-categories" href={`/blog/${i}`}>{i + '\u00A0'}</a>
            )
        })
            return (
                <div key={item.id} className="col-lg-6 col-md-6 col-12">
                    <div className="single-blog">
                        <div className="blog-img">
                            <Link to={`/blog/${item.id}`}>
                                <img src={`/assets/main/images/blog/${item.img}`} alt={item.title} />
                            </Link>
                        </div>
                        <div className="blog-content">
                            {newCategory}
                            <h4>
                                <Link to={`/blog/${item.id}`}>{item.title}</Link>
                            </h4>
                            <div className="button">
                                <Link to={`/blog/${item.id}`} className="btn">Read More</Link>
                            </div>
                        </div>
                    </div>
                </div>
            )
    })
    return (
        <>
            {mapData}
        </>
    )
}
