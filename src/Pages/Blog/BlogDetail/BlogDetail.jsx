import React from 'react'
import { useState, useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import Loading from '../../../Components/Loading';
import Layout from '../../../Components/Shop/Layout/Layout'
import SideBar from '../SideBar/SideBar'
import { get } from '../../../services/HttpClient';
import Comment from './Comment';
import '../style.css'

export default function BlogDetail({ cartItems }) {
    const [data, setData] = useState([])
    const [loader, setLoader] = useState(true)
    const postId = useParams()
    useEffect(() => {
        get('/blog/blogAPI.php')
            .then(response => {
                setData(response)
                setLoader(false)
            })
    }, []);

    const mapData = data && data.filter(a => a.id === postId.id).map(itm => itm)
    const item = mapData[0]
    const tags = item && item.tag.split(',')
    const mapTags = tags && tags.map((item, index) => {
        return (
            <li key={index}><Link to={`/blog/tag/${item}`}>{item}</Link></li>
        )
    })
    return (
        <>
            <Layout cartItems={cartItems} pgTitle='Detail'>
                {loader ? <Loading /> :
                    <section className="section blog-single">
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-8 col-md-12 col-12">
                                    <div className="single-inner">
                                        <div className="post-details">
                                            <div className="main-content-head">
                                                <div className="post-thumbnils">
                                                    <img src={`/assets/main/images/blog/${item.img}`} alt={item.title} />
                                                </div>
                                                <div className="meta-information">
                                                    <h2 className="post-title">
                                                        <a href="blog-single.html">{item.title}</a>
                                                    </h2>
                                                    <ul className="meta-info">
                                                        <li>
                                                            <span> <i className="lni lni-user"></i> Admin</span>
                                                        </li>
                                                        <li>
                                                            <span><i className="lni lni-calendar"></i> {item.createdAt.slice(0, 10)}</span>
                                                        </li>
                                                        <li>
                                                            <a href="true"><i className="lni lni-tag"></i> {item.category}</a>
                                                        </li>
                                                        <li>
                                                            <span><i className="lni lni-timer"></i> 5 min read</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div className="detail-inner">
                                                    <p>{item.text}</p>
                                                    <hr />
                                                    <div className="post-bottom-area">
                                                        <div className="post-tag">
                                                            <span className='tag-title-blog-detail'>Tags:</span>
                                                            <ul>
                                                                {mapTags}
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <Comment />
                                        </div>
                                    </div>
                                </div>
                                <SideBar data={data} />
                            </div>
                        </div>
                    </section>
                }
            </Layout>
        </>
    )
}
