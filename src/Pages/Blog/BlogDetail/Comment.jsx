import React from 'react'

export default function Comment() {
    return (
        <>
            <div className="post-comments">
                <h3 className="comment-title"><span>Post comments</span></h3>
                <ul className="comments-list">
                    <li>
                        <div className="comment-img">
                            <img src="assets/images/blog/comment1.jpg" alt="img" />
                        </div>
                        <div className="comment-desc">
                            <div className="desc-top">
                                <h6>Arista Williamson</h6>
                                <span className="date">19th May 2023</span>
                                <a href="javascript:void(0)" className="reply-link"><i
                                    className="lni lni-reply"></i>Reply</a>
                            </div>
                            <p>
                                Donec aliquam ex ut odio dictum, ut consequat leo interdum. Aenean nunc
                                ipsum, blandit eu enim sed, facilisis convallis orci. Etiam commodo
                                lectus
                                quis vulputate tincidunt. Mauris tristique velit eu magna maximus
                                condimentum.
                            </p>
                        </div>
                    </li>
                    <li className="children">
                        <div className="comment-img">
                            <img src="assets/images/blog/comment2.jpg" alt="img" />
                        </div>
                        <div className="comment-desc">
                            <div className="desc-top">
                                <h6>Rosalina Kelian</h6>
                                <span className="date">15th May 2023</span>
                                <a href="javascript:void(0)" className="reply-link"><i
                                    className="lni lni-reply"></i>Reply</a>
                            </div>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                tempor incididunt ut labore et dolore magna aliqua. Ut enim.
                            </p>
                        </div>
                    </li>
                    <li>
                        <div className="comment-img">
                            <img src="assets/images/blog/comment3.jpg" alt="img" />
                        </div>
                        <div className="comment-desc">
                            <div className="desc-top">
                                <h6>Alex Jemmi</h6>
                                <span className="date">12th May 2023</span>
                                <a href="javascript:void(0)" className="reply-link"><i
                                    className="lni lni-reply"></i>Reply</a>
                            </div>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
                                veniam.
                            </p>
                        </div>
                    </li>
                </ul>
            </div>
            <div className="comment-form">
                <h3 className="comment-reply-title">Leave a comment</h3>
                <form action="#" method="POST">
                    <div className="row">
                        <div className="col-lg-6 col-12">
                            <div className="form-box form-group">
                                <input type="text" name="name" className="form-control form-control-custom"
                                    placeholder="Website URL" />
                            </div>
                        </div>
                        <div className="col-lg-6 col-12">
                            <div className="form-box form-group">
                                <input type="text" name="email" className="form-control form-control-custom"
                                    placeholder="Your Name" />
                            </div>
                        </div>
                        <div className="col-lg-6 col-12">
                            <div className="form-box form-group">
                                <input type="email" name="email"
                                    className="form-control form-control-custom" placeholder="Your Email" />
                            </div>
                        </div>
                        <div className="col-lg-6 col-12">
                            <div className="form-box form-group">
                                <input type="text" name="name" className="form-control form-control-custom"
                                    placeholder="Phone Number" />
                            </div>
                        </div>
                        <div className="col-12">
                            <div className="form-box form-group">
                                <textarea name="#" className="form-control form-control-custom"
                                    placeholder="Your Comments"></textarea>
                            </div>
                        </div>
                        <div className="col-12">
                            <div className="button">
                                <button type="submit" className="btn">Post Comment</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </>
    )
}
