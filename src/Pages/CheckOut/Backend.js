import { useState } from 'react'
import { toast } from 'react-toastify'
import { useGetDate } from '../../CustomHooks/useGetDate'
import { useNavigate } from 'react-router-dom'
import { post } from '../../services/HttpClient'
import { User_loggedin } from '../../Context/User_loggedin'

export function Backend(cartProducts, totalPrice) {
    const [shipPrice, setShipPrice] = useState(0)
    const [shipType, setShipType] = useState('')
    const [total, setTotal] = useState(totalPrice)
    const [person, setPerson] = useState([])
    const randomNumber = Math.floor(Math.random() * 9999999999) + 1
    const dateNow = useGetDate();
    let navigate = useNavigate();
    const userInfo = User_loggedin()

    // Handle TotalPrice + ShippPrice
    const totalPriceShipping = (price, type) => {
        setTotal(totalPrice + price)
        setShipPrice(price)
        setShipType(type)
    }

    // Fetch All PersonalData From Form
    const personalData = (data) => {
        setPerson(data)
    }

    // Add New Items To Add In Person Information
    const randomTransaction = 'DGW' + randomNumber;
    const newItems = {
        transId: randomTransaction,
        datePaying: dateNow,
        totalPrice: total,
        shipType: shipType,
        productsInfo: cartProducts
    }
    const newPerson = { ...person, newItems }

    // Values For Insert Datas In Database (API)
    const payload = {
        transactionsID: randomTransaction,
        modeOfPayment: 'PayPal',
        modeOfShipping: shipType,
        firstName: person.firstName,
        lastName: person.lastName,
        username: userInfo.username,
        mobile: person.phone,
        email: person.email,
        country: person.country,
        city: person.city,
        address: person.address,
        postCode: person.postCode,
        products: 'X',
        totalPrice: total,
        stats: 1,
    }

    // Handle Click Paying
    const paying = () => {
        if (newPerson.firstName && newPerson.lastName && newPerson.email && newPerson.phone && newPerson.address && newPerson.city && newPerson.postCode && newPerson.country) {
            post('/paying/insertCartAPI.php',
                JSON.stringify(payload)).then(res => {

                })
            navigate("/paying/success", { state: newPerson });
        }
        else {
            toast['error']('Please fill all of the fields.')
        }
    }

    return { totalPriceShipping, personalData, totalPrice, total, shipPrice, paying }
}
