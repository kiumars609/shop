import React, { useEffect } from 'react'
import Layout from '../../Components/Shop/Layout/Layout'
import LeftSide from './LeftSide/LeftSide'
import RightSide from './RightSide/RightSide'
import Tostify from '../../Components/Tostify'
import { Backend } from './Backend'
import { Link, useParams } from 'react-router-dom'
import { get } from '../../services'
import { useState } from 'react'
import { User_loggedin } from '../../Context/User_loggedin'
import './style.css'

export default function CheckOut({ cartItems }) {

    const userInfo = User_loggedin()

    const singleProduct = useParams();
    const [itm, setItm] = useState([])

    useEffect(() => {
        if (singleProduct.type) {
            get(`/products/${singleProduct.category}/${singleProduct.category}API.php`)
                .then(response => {
                    setItm(response.filter(a => a.id === singleProduct.id))
                })
        }
    }, [singleProduct.category, singleProduct.id, singleProduct]);

    const totalPrice = singleProduct.type ? itm.reduce((price, item) => price + singleProduct.quantity * item.price, 0) : cartItems.reduce((price, item) => price + item.quantity * item.price, 0);
    const cartProducts = singleProduct.type ? itm : cartItems
    const back = Backend(cartProducts, totalPrice);
    return (
        <>
            <Layout pgTitle='CheckOut' cartItems={cartItems}>
                <Tostify position='top-center' width='350px' />
                <section className="checkout-wrapper section">
                    <div className="container">
                        <div className="row justify-content-center">
                            {userInfo
                                ?
                                <>
                                    <LeftSide totalPriceShipping={back.totalPriceShipping} personalData={back.personalData} />
                                    <RightSide totalPrice={back.totalPrice} total={back.total} shipPrice={back.shipPrice} paying={back.paying} />
                                </>
                                :
                                <div className="col-md-8 main-login-animate shadow">
                                    <p className='mx-auto text-center mt-5 fs-3 text'>
                                        <span className='please'>Please</span> <Link to={'/login'} className='login'>Login</Link> <span className='or'>or</span> <Link to={'/register'} className='register'>Register</Link>
                                    </p>
                                </div>
                            }
                        </div>
                    </div>
                </section>
            </Layout>
        </>
    )
}
