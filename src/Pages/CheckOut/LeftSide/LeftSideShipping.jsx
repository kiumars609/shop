import React from 'react'

export default function LeftSideShipping({ totalPriceShipping }) {
    return (
        <>
            <li>
                <h6 className="title collapsed" data-bs-toggle="collapse" data-bs-target="#collapseFour"
                    aria-controls="collapseFour">Delivery Option</h6>
                <section className="checkout-steps-form-content collapse" id="collapseFour"
                    aria-labelledby="headingFour" data-bs-parent="#accordionExample">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="checkout-payment-option">
                                <div className="payment-option-wrapper">

                                    <div className="single-payment-option" onClick={() => totalPriceShipping(5, 'Standerd Shipping')}>
                                        <input type="radio" name="shipping" id="shipping-1" />
                                        <label htmlFor="shipping-1">
                                            <img src="/assets/main/images/shipping/shipping-1.png"
                                                alt="Sipping" />
                                            <p>Standerd Shipping</p>
                                            <span className="price">€5</span>
                                        </label>
                                    </div>
                                    <div className="single-payment-option" onClick={() => totalPriceShipping(10, 'DHL Shipping')}>
                                        <input type="radio" name="shipping" id="shipping-2" />
                                        <label htmlFor="shipping-2">
                                            <img src="/assets/main/images/shipping/shipping-2.png"
                                                alt="Sipping" />
                                            <p>DHL Shipping</p>
                                            <span className="price">€10</span>
                                        </label>
                                    </div>
                                    <div className="single-payment-option" onClick={() => totalPriceShipping(12, 'dpd Shipping')}>
                                        <input type="radio" name="shipping" id="shipping-3" />
                                        <label htmlFor="shipping-3">
                                            <img src="/assets/main/images/shipping/shipping-3.png"
                                                alt="Sipping" />
                                            <p>dpd Shipping</p>
                                            <span className="price">€12</span>
                                        </label>
                                    </div>
                                    <div className="single-payment-option" onClick={() => totalPriceShipping(15, 'InPost Shipping')}>
                                        <input type="radio" name="shipping" id="shipping-4" />
                                        <label htmlFor="shipping-4">
                                            <img src="/assets/main/images/shipping/shipping-4.png"
                                                alt="Sipping" />
                                            <p>InPost Shipping</p>
                                            <span className="price">€15</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-12">
                            <div className="steps-form-btn button">
                                <button className="btn" data-bs-toggle="collapse"
                                    data-bs-target="#collapseThree" aria-expanded="false"
                                    aria-controls="collapseThree">previous</button>
                            </div>
                        </div>
                    </div>
                </section>
            </li>
        </>
    )
}
