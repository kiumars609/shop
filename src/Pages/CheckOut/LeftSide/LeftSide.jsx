import React from 'react'
// import LeftSidePayment from './LeftSidePayment'
import LeftSidePersonalDetail from './LeftSidePersonalDetail'
import LeftSideShipping from './LeftSideShipping'
import { User_loggedin } from '../../../Context/User_loggedin'

export default function LeftSide({ totalPriceShipping, personalData }) {

    const userInfo = User_loggedin()
    return (
        <>
            <div className="col-lg-8">
                <div className="checkout-steps-form-style-1">
                    <ul id="accordionExample">
                        <LeftSidePersonalDetail personalData={personalData} userInfo={userInfo} />
                        <LeftSideShipping totalPriceShipping={totalPriceShipping} />
                        {/* <LeftSidePayment /> */}
                    </ul>
                </div>
            </div>
        </>
    )
}
