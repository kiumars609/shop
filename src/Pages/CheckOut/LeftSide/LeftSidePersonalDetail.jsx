import React from 'react'
import { useState, useEffect,useLayoutEffect } from 'react'

export default function LeftSidePersonalDetail({ personalData,userInfo }) {

    // console.log(jende && true);
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [phone, setPhone] = useState('');
    const [address, setAddress] = useState('');
    const [city, setCity] = useState('');
    const [postCode, setPostCode] = useState('');
    const [country, setCountry] = useState('');
    // const [region, setRegion] = useState(null);
    // const [sameDelivery, setSameDelivery] = useState(false);

    useEffect(() => {
        if(userInfo){
            setFirstName(userInfo.fname)
            setLastName(userInfo.lname)
            setEmail(userInfo.email)
            setPhone(userInfo.phone)
            personalData({ firstName, lastName, email, phone, address, city, postCode, country })
        }
        else{
            personalData({ firstName, lastName, email, phone, address, city, postCode, country })
        }
    }, [firstName, lastName, email, phone, address, city, postCode, country]);
    return (
        <>
            <li>
                <h6 className="title" data-bs-toggle="collapse" data-bs-target="#collapseThree"
                    aria-controls="collapseThree">Your Personal Details </h6>
                <section className="checkout-steps-form-content collapse show" id="collapseThree"
                    aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                    <div className="row">
                        <div className="col-md-6">
                            <div className="single-form form-default">
                                <label>First Name</label>
                                <div className="form-input form">
                                    <input type="text" placeholder="" value={userInfo ? userInfo.fname : firstName} disabled={userInfo && "disabled"} onChange={(e) => setFirstName(e.target.value)} />
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="single-form form-default">
                                <label>Last Name</label>
                                <div className="form-input form">
                                    <input type="text" placeholder="" value={userInfo ? userInfo.lname : lastName} disabled={userInfo && "disabled"} onChange={(e) => setLastName(e.target.value)} />
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="single-form form-default">
                                <label>Email Address</label>
                                <div className="form-input form">
                                    <input type="text" placeholder="" value={userInfo ? userInfo.email : email} disabled={userInfo && "disabled"} onChange={(e) => setEmail(e.target.value)} />
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="single-form form-default">
                                <label>Phone Number</label>
                                <div className="form-input form">
                                    <input type="text" placeholder="" value={userInfo ? userInfo.phone : phone} disabled={userInfo && "disabled"} onChange={(e) => setPhone(e.target.value)} />
                                </div>
                            </div>
                        </div>
                        <div className="col-md-12">
                            <div className="single-form form-default">
                                <label>Mailing Address</label>
                                <div className="form-input form">
                                    <input type="text" placeholder="" value={address} onChange={(e) => setAddress(e.target.value)} />
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="single-form form-default">
                                <label>City</label>
                                <div className="form-input form">
                                    <input type="text" placeholder="" value={city} onChange={(e) => setCity(e.target.value)} />
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="single-form form-default">
                                <label>Post Code</label>
                                <div className="form-input form">
                                    <input type="text" placeholder="" value={postCode} onChange={(e) => setPostCode(e.target.value)} />
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="single-form form-default">
                                <label>Country</label>
                                <div className="form-input form">
                                    <input type="text" placeholder="" value={country} onChange={(e) => setCountry(e.target.value)} />
                                </div>
                            </div>
                        </div>
                        <div className="col-md-12">
                            <div className="single-form button">
                                <button className="btn" data-bs-toggle="collapse"
                                    data-bs-target="#collapseFour" aria-expanded="false"
                                    aria-controls="collapseFour">next
                                    step</button>
                            </div>
                        </div>
                    </div>
                </section>
            </li>
        </>
    )
}
