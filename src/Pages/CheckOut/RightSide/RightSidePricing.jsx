import React from 'react'
import { useCommaSeparators } from '../../../CustomHooks/useCommaSeparators'

export default function RightSidePricing({ totalPrice, total, shipPrice, paying }) {

    return (
        <>
            <div className="checkout-sidebar-price-table mt-30">
                <h5 className="title">Pricing Table</h5>
                <div className="sub-total-price">
                    <div className="total-price">
                        <p className="value">Subotal Price:</p>
                        <p className="price">€{useCommaSeparators(totalPrice)}</p>
                    </div>
                </div>
                <div className="sub-total-price">
                    <div className="total-price">
                        <p className="value">Shipping Price:</p>
                        <p className="price">€{useCommaSeparators(shipPrice)}</p>
                    </div>
                </div>
                <div className="total-payable">
                    <div className="payable-price">
                        <p className="value">Total Price:</p>
                        <p className="price">€{useCommaSeparators(total)}</p>
                    </div>
                </div>
                <div className="price-table-btn button">
                    <button className="btn" onClick={() => paying()}>Pay Now</button>
                </div>
            </div>
        </>
    )
}
