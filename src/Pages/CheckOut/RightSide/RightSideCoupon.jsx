import React from 'react'

export default function RightSideCoupon() {
    return (
        <>
            <div className="checkout-sidebar-coupon">
                <p>Appy Coupon to get discount!</p>
                <form action="#">
                    <div className="single-form form-default">
                        <div className="form-input form">
                            <input type="text" placeholder="Coupon Code" />
                        </div>
                        <div className="button">
                            <button className="btn">apply</button>
                        </div>
                    </div>
                </form>
            </div>
        </>
    )
}
