import React from 'react'
// import RightSideAds from './RightSideAds'
import RightSideCoupon from './RightSideCoupon'
import RightSidePricing from './RightSidePricing'

export default function RightSide({ totalPrice, total, shipPrice, paying }) {
    return (
        <>
            <div className="col-lg-4">
                <div className="checkout-sidebar">
                    <RightSideCoupon />
                    <RightSidePricing totalPrice={totalPrice} total={total} shipPrice={shipPrice} paying={paying} />
                    {/* <RightSideAds /> */}
                </div>
            </div>
        </>
    )
}
