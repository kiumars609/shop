import React from 'react'

export default function RightSideAds() {
    return (
        <>
            <div className="checkout-sidebar-banner mt-30">
                <a href="product-grids.html">
                    <img src="/assets/main/images/banner/banner.jpg" alt="#" />
                </a>
            </div>
        </>
    )
}
