import React from 'react'
import { Link } from 'react-router-dom'
import { useCommaSeparators } from '../../CustomHooks/useCommaSeparators'

export default function CartSingle({ data, handleAddProduct, handleRemoveProduct }) {
    return (
        <>
            <div className="cart-single-list">
                <div className="row align-items-center">
                    <div className="col-lg-1 col-md-1 col-12">
                        <Link to={`/product/${data.category}/${data.id}`}><img src={`/assets/main/images/products/${data.category}/${data.img1}`} alt={data.titleSmall} /></Link>
                    </div>
                    <div className="col-lg-4 col-md-3 col-12">
                        <h5 className="product-name"><Link to={`/product/${data.category}/${data.id}`}>{data.titleSmall}</Link></h5>
                        <p className="product-des">
                            <span><em>Category:</em> {data.category}</span>
                            <span><em>Color:</em> Black</span>
                        </p>
                    </div>
                    <div className="col-lg-2 col-md-2 col-12">
                        <div className="count-input">
                            <button className='btn cart-items-add' onClick={() => handleAddProduct(data)}>+</button>
                            <span>{data.quantity}</span>
                            <button className='btn cart-items-remove' onClick={() => handleRemoveProduct(data)}>-</button>
                        </div>
                    </div>
                    <div className="col-lg-2 col-md-2 col-12">
                        <p>€{useCommaSeparators(data.price)}</p>
                    </div>
                    <div className="col-lg-2 col-md-2 col-12">
                        <p>-</p>
                    </div>
                    <div className="col-lg-1 col-md-2 col-12">
                        <a className="remove-item" href="true"><i className="lni lni-close"></i></a>
                    </div>
                </div>
            </div>
        </>
    )
}
