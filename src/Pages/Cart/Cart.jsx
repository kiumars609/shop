import React from 'react'
import { Link } from 'react-router-dom'
import Layout from '../../Components/Shop/Layout/Layout'
import { useCommaSeparators } from '../../CustomHooks/useCommaSeparators'
import CartSingle from './CartSingle'

export default function Cart({ cartItems, handleAddProduct, handleRemoveProduct, handleCartClearance }) {

    const totalPrice = cartItems.reduce((price, item) => price + item.quantity * item.price, 0)
    return (
        <Layout pgTitle='Cart' cartItems={cartItems}>
            <div className="shopping-cart section">
                <div className="container">
                    <div className="cart-list-head">
                        <div className="cart-list-title">
                            <div className="row">
                                <div className="col-lg-1 col-md-1 col-12">
                                </div>
                                <div className="col-lg-4 col-md-3 col-12">
                                    <p>Product Name</p>
                                </div>
                                <div className="col-lg-2 col-md-2 col-12">
                                    <p>Quantity</p>
                                </div>
                                <div className="col-lg-2 col-md-2 col-12">
                                    <p>Subtotal</p>
                                </div>
                                <div className="col-lg-2 col-md-2 col-12">
                                    <p>Discount</p>
                                </div>
                                <div className="col-lg-1 col-md-2 col-12">
                                    <p>Remove</p>
                                </div>
                            </div>
                        </div>
                        {cartItems.length === 0 && <div className='col-md-12 p-3'>No Items</div>}
                        {cartItems.map((item,index) => {
                            return <CartSingle key={index} data={item} handleAddProduct={handleAddProduct} handleRemoveProduct={handleRemoveProduct} />
                        })}
                        {cartItems.length >= 1 && (
                            <button className='btn btn-danger m-1' onClick={() => handleCartClearance()}>Clear Cart</button>
                        )}
                    </div>
                    <div className="row">
                        <div className="col-12">
                            <div className="total-amount">
                                <div className="row">
                                    <div className="col-lg-4 col-md-6 col-12">
                                        <div className="right">
                                            <ul>
                                                <li>Cart Subtotal<span>€{useCommaSeparators(totalPrice)}</span></li>
                                                <li>Shipping<span>Free</span></li>
                                                <li>You Save<span>€0</span></li>
                                                <li className="last">You Pay<span>€0</span></li>
                                            </ul>
                                            <div className="button">
                                                <Link to='/checkout' className='btn'>Checkout</Link>
                                                <Link to='/' className="btn btn-alt">Continue shopping</Link>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    )
}
