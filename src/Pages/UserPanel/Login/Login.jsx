import React from 'react'
import { useLayoutEffect } from 'react'
import { useState, useEffect } from 'react'
import { Link, Navigate } from 'react-router-dom'
import Layout from '../../../Components/Shop/Layout'
import { useAuthDispatcher, useAuthState } from '../../../Context/auth-context'
import { actionType } from '../../../Context/reducer'
import { get } from '../../../services'
import Loading from '../../../Components/Loading'

export default function Login({ cartItems }) {

    const { token } = useAuthState()
    const { loading } = useAuthState()
    const dispatch = useAuthDispatcher()
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [userToken, setUserToken] = useState(null)
    const handleSubmit = (e) => {
        e.preventDefault()
        dispatch({
            type: actionType.LOGIN_REQUEST,
        })
        get('/users/getUserAPI.php')
            .then(response => {
                const ui = response.filter(user => user.username === username && user.password === password)
                setUserToken(ui[0]['tokens'])
            })
    }

    useLayoutEffect(() => {
        const toks = localStorage.getItem('token')
        if (toks) {
            dispatch({
                type: actionType.LOGIN_REQUEST,
            })
            setUserToken(toks)
        }
    }, [dispatch]);

    useEffect(() => {
        if (userToken) {
            get('/users/getUserAPI.php')
                .then(response => {
                    const uii = response.filter(user => user.tokens === userToken)
                    if (uii) {
                        localStorage.setItem('token', uii[0]['tokens'])
                        dispatch({
                            type: actionType.LOGIN_SUCCESS,
                            payload: {
                                user: uii[0]['username'],
                                token: uii[0]['tokens']
                            }
                        })
                    }
                })
        }
    }, [userToken, dispatch]);
    return (
        <>
            {!token
                ?
                loading ? <Loading />
                    :
                    <Layout cartItems={cartItems} pgTitle='Login Panel'>
                        <div className="account-login section">
                            <div className="container">
                                <div className="row">
                                    <div className="col-lg-6 offset-lg-3 col-md-10 offset-md-1 col-12">
                                        <form className="card login-form" onSubmit={handleSubmit}>
                                            <div className="card-body">
                                                <div className="title">
                                                    <h3>Login Now</h3>
                                                </div>
                                                <div className="form-group input-group">
                                                    <label htmlFor="reg-fn">Username</label>
                                                    <input className="form-control" type="text" id="reg-username" value={username} onChange={(e) => setUsername(e.target.value)} />
                                                </div>
                                                <div className="form-group input-group">
                                                    <label htmlFor="reg-fn">Password</label>
                                                    <input className="form-control" type="password" id="reg-pass" value={password} onChange={(e) => setPassword(e.target.value)} />
                                                </div>
                                                <div className="d-flex flex-wrap justify-content-between bottom-content">
                                                    <div className="form-check">
                                                        <input type="checkbox" className="form-check-input width-auto" id="exampleCheck1" />
                                                        <label className="form-check-label">Remember me</label>
                                                    </div>
                                                    <a className="lost-pass" href="account-password-recovery.html">Forgot password?</a>
                                                </div>
                                                <div className="button">
                                                    <button className="btn" type="submit">Login</button>
                                                </div>
                                                <p className="outer-link">Don't have an account? <Link to={'/register'}>Register here </Link>
                                                </p>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Layout>
                :
                <Navigate to="/profile" replace />
            }
        </>
    )
}
