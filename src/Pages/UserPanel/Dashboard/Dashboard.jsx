import React, { useEffect, useState } from 'react'
import Layout from '../../../Components/Profile/Layout/Layout'
import { User_loggedin } from '../../../Context/User_loggedin'
import { get } from '../../../services'
import MyOrders from './MyOrders'
import YourPurchases from './YourPurchases'

export default function Dashboard() {

  const userInfo = User_loggedin()
  const [data, setData] = useState([])
  const [loading, setLoading] = useState(true)
  useEffect(() => {
    get('/paying/getCartAPI.php')
      .then(response => {
        setData(response)
        setLoading(false)
      })
  }, []);

  return (
    <>
      <Layout title={'Dashboard'}>
        {loading
          ?
          'Loading' :
          <MyOrders data={data} userInfo={userInfo} />
        }
        <YourPurchases />
      </Layout>
    </>
  )
}
