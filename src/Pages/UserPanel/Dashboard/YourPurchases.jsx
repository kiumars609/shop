import React from 'react'

export default function YourPurchases() {
    return (
        <>
            <div className="card your-purchases">
                <div className="card-header">
                    <h5 className="card-title mb-0">
                        Your purchases
                        <span className='line'></span>
                    </h5>
                    <a href='true' className='see-all'>
                        see all
                        <i className="fa fa-chevron-right" aria-hidden="true"></i>
                    </a>
                </div>
                <div className="card-body row">
                    <div className="card product col-3">
                        <a href='true'>
                            <img src="/assets/main/images/icons/comingSoon.png" className="card-img-top" alt="..." />
                            <div className="card-body">
                                <h5 className="card-title">Audi R8</h5>
                                <p className="card-text text-dark">$150</p>
                                {/* <a href="true" className="btn btn-primary">Go somewhere</a> */}
                            </div>
                        </a>
                    </div>
                    <div className="card product col-3">
                        <a href='true'>
                            <img src="/assets/main/images/icons/comingSoon.png" className="card-img-top" alt="..." />
                            <div className="card-body">
                                <h5 className="card-title">Audi R8</h5>
                                <p className="card-text text-dark">$150</p>
                                {/* <a href="true" className="btn btn-primary">Go somewhere</a> */}
                            </div>
                        </a>
                    </div>
                    <div className="card product col-3">
                        <a href='true'>
                            <img src="/assets/main/images/icons/comingSoon.png" className="card-img-top" alt="..." />
                            <div className="card-body">
                                <h5 className="card-title">Audi R8</h5>
                                <p className="card-text text-dark">$150</p>
                                {/* <a href="true" className="btn btn-primary">Go somewhere</a> */}
                            </div>
                        </a>
                    </div>
                    <div className="card product col-3">
                        <a href='true'>
                            <img src="/assets/main/images/icons/comingSoon.png" className="card-img-top" alt="..." />
                            <div className="card-body">
                                <h5 className="card-title">Audi R8</h5>
                                <p className="card-text text-dark">$150</p>
                                {/* <a href="true" className="btn btn-primary">Go somewhere</a> */}
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </>
    )
}
