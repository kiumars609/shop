import React from 'react'
import { useState } from 'react'
import { Link } from 'react-router-dom'

export default function MyOrders({ data, userInfo }) {

    const [proccssing, setProccssing] = useState([])

    console.log(data);

    const mapItems = data && data.map((item, index) => {
        if (item.username === userInfo.username) {
            if (item.status === '1') {
                return item
                // setProccssing(item)

            }
        }
    })

    console.log(mapItems.length);

    return (
        <>
            <div className="card my-orders">
                <div className="card-header">
                    <h5 className="card-title mb-0">
                        My orders
                        <span className='line'></span>
                    </h5>
                    <Link to={'/profile/orders'} className='see-all'>
                        see all
                        <i className="fa fa-chevron-right" aria-hidden="true"></i>
                    </Link>
                </div>
                <div className="card-body row">
                    <div className="col-4 position-relative">
                        <img src='/assets/main/images/icons/status-processing.svg' alt='' />
                        <br />
                        <span>Processing</span>
                        <span className='status-count'>100</span>
                    </div>
                    <div className="col-4 position-relative">
                        <img src='/assets/main/images/icons/status-delivered.svg' alt='' />
                        <br />
                        <span>Delivered</span>
                        <span className='status-count delivered'>20</span>
                    </div>
                    <div className="col-4 position-relative">
                        <img src='/assets/main/images/icons/status-returned.svg' alt='' />
                        <br />
                        <span>Returned</span>
                        <span className='status-count delivered'>6</span>
                    </div>
                </div>
            </div>
        </>
    )
}
