import React from 'react'

export default function Current({ items, userInfo }) {

    console.log(userInfo);
    const mapItems = items && items.map((item, index) => {
        if (item.username === userInfo.username) {
            return (
                <div key={index} className="col-md-11 main-orders">
                    <p>
                        {item.transactionsID}
                    </p>
                </div>
            )
        }
    })
    return (
        <>
            {mapItems}
        </>
    )
}
