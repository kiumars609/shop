import React, { useEffect } from 'react'
import { useState } from 'react';
import { useLocation, useParams } from 'react-router-dom'
import { get } from '../../../services';
import Pdf from "react-to-pdf";


export default function Invoice() {
    const user = useParams();
    // console.log(user.id);
    const [item, setItem] = useState([])

    const ref = React.createRef();
    let locat = useLocation()
    const itmm = locat.state;

    useEffect(() => {
        get('/paying/getCartAPI.php')
            .then(response => {
                const data = response.find(id => id.transactionsID === user.id)
                setItem(data)
            })
    }, [user.id]);

    console.log(item);
    return (
        <>
            <div className="col-md-10 mx-auto mt-2 d-flex p-3 justify-content-between invoice-header">
                <img src='/assets/main/images/logo/logo2.png' alt='digi-ware' className='col-md-2' />
                <button className='btn btn-danger'>Print / Download</button>

                <Pdf targetRef={ref} filename={`receipt-test.pdf`}>
                    {({ toPdf }) =>
                        <div className="col-4"><i className="lni lni-adobe"></i> <span role='button' className='bottom-links' onClick={toPdf}>Save as PDF</span></div>
                    }
                </Pdf>
            </div>

            <div className="col-md-8 mx-auto mt-5">
                <h3 className='text-center'>Electronic bill of sale</h3>

                <table class="table table-bordered mt-3">
                    <tbody>
                        <tr>
                            <td className='col-md-1 text-center bg-secondary text-light'>Customer</td>
                            <td>
                                <div className="row mx-auto">
                                    <div className="col-4">Buyer: {item.firstName + ' ' + item.lastName}</div>
                                    <div className="col-4">Mobile: {item.mobile}</div>
                                    <div className="col-4">Email: {item.email}</div>

                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td className='col-md-1 text-center bg-secondary text-light'>Location</td>
                            <td>
                                <div className="row mx-auto">
                                    <div className="col-4">Country: {item.country}</div>
                                    <div className="col-4">City: {item.city}</div>
                                    <div className="col-4">Post Code: {item.postCode}</div>
                                    <div className="col-12">Address: {item.address}</div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td className='col-md-1 text-center bg-secondary text-light'>Product</td>
                            <td>
                                <div className="row mx-auto">
                                    <div className="col-4">Payment: {item.modeOfPayment}</div>
                                    <div className="col-4">Shipping: {item.modeOfShipping}</div>
                                    <div className="col-4">Transactions ID: {item.transactionsID}</div>
                                    <div className="col-4">Date Paying: {item.datePaying}</div>
                                    <div className="col-4">Total Price: €{item.totalPrice}</div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </>
    )
}
