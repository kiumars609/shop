import React from 'react'

export default function Delivery({ value }) {
    if (value === 'Standerd Shipping') {
        return <img src="/assets/main/images/shipping/shipping-1.png" alt="Sipping" />
    }
    if (value === 'DHL Shipping') {
        return <img src="/assets/main/images/shipping/shipping-2.png" alt="Sipping" />
    }
    if (value === 'dpd Shipping') {
        return <img src="/assets/main/images/shipping/shipping-3.png" alt="Sipping" />
    }
    if (value === 'InPost Shipping') {
        return <img src="/assets/main/images/shipping/shipping-4.png" alt="Sipping" />
    }
}
