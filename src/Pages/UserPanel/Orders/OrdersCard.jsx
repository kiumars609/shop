import React from 'react'
import { Link } from 'react-router-dom';
import Delivery from './Delivery';
import Statuses from './Statuses'

export default function OrdersCard({ items, userInfo, status }) {

    const mapItems = items && items.map((item, index) => {
        if (item.username === userInfo.username) {
            if (item.status === status) {
                return (
                    <div key={index} className="col-md-11 main-orders">
                        <p className='position-relative'>
                            <span><Statuses value={item.status} /></span>
                            <br /><br />
                            <span>12/25/2022</span>
                            <span className='seprate'></span>
                            <span>Order code: <span className='result'>{item.transactionsID}</span></span>
                            <span className='seprate'></span>
                            <span>Amount: <span className='result'>€{item.totalPrice}</span></span>
                            <hr />
                            <span>Delivery: <Delivery value={item.modeOfShipping} /></span>
                            <hr />
                            <div className="col-12 d-flex flex-row-reverse">
                                <Link to={`/profile/invoice/${item.transactionsID}`}>
                                    <span className='text-info invoice'>View the invoice <i class="fa fa-file-text fs-5" aria-hidden="true"></i></span>
                                </Link>
                            </div>
                        </p>
                    </div>
                )
            }
        }
    })
    return (
        <>
            {mapItems}
        </>
    )
}

