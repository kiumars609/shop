import React from 'react'
import { useState, useEffect } from 'react';
import Layout from '../../../Components/Profile/Layout/Layout'
import { User_loggedin } from '../../../Context/User_loggedin';
import { get } from '../../../services';
import Current from './Current';
import OrdersCard from './OrdersCard';
import './style.css'

export default function Orders() {

    const userInfo = User_loggedin()
    const [data, setData] = useState([])
    const [loading, setLoading] = useState(true)
    useEffect(() => {
        get('/paying/getCartAPI.php')
            .then(response => {
                setData(response)
                setLoading(false)
            })
    }, []);
    return (
        <>
            <Layout title={'Orders'}>
                <div className="col-md-12 bg-white shadow rounded p-3">
                    <h5>Order history</h5>
                    <nav className='mt-5'>
                        <div className="nav nav-tabs" id="nav-tab" role="tablist">
                            <button className="nav-link active" id="nav-current-tab" data-bs-toggle="tab" data-bs-target="#nav-current" type="button" role="tab" aria-controls="nav-current" aria-selected="true">Current</button>
                            <button className="nav-link" id="nav-delivered-tab" data-bs-toggle="tab" data-bs-target="#nav-delivered" type="button" role="tab" aria-controls="nav-delivered" aria-selected="false">Delivered</button>
                            <button className="nav-link" id="nav-returned-tab" data-bs-toggle="tab" data-bs-target="#nav-returned" type="button" role="tab" aria-controls="nav-returned" aria-selected="false">Returned</button>
                            <button className="nav-link" id="nav-canceled-tab" data-bs-toggle="tab" data-bs-target="#nav-canceled" type="button" role="tab" aria-controls="nav-canceled" aria-selected="true">Canceled</button>
                        </div>
                    </nav>
                    <div className="tab-content" id="nav-tabContent">
                        <div className="tab-pane fade show active" id="nav-current" role="tabpanel" aria-labelledby="nav-current-tab" tabIndex="0">
                            {loading
                                ?
                                'Loading' :
                                <OrdersCard items={data} userInfo={userInfo} status={"1"} />
                            }
                        </div>
                        <div className="tab-pane fade" id="nav-delivered" role="tabpanel" aria-labelledby="nav-delivered-tab" tabIndex="0">...</div>
                        <div className="tab-pane fade" id="nav-returned" role="tabpanel" aria-labelledby="nav-returned-tab" tabIndex="0">...</div>
                        <div className="tab-pane fade" id="nav-canceled" role="tabpanel" aria-labelledby="nav-canceled-tab" tabIndex="0">...</div>
                    </div>
                </div>
            </Layout>
        </>
    )
}
