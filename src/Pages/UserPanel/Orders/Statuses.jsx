import React from 'react'

export default function Statuses({ value }) {
    if (value === "1") {
        return (<><i class="fa fa-exclamation fs-6 bg-warning p-1 current-icon"></i> <span>Current</span></>)
    }

    if (value === "2") {
        return (<><i class="fa fa-exclamation fs-6 bg-success p-1 current-icon"></i> <span>Delivered</span></>)
    }

    if (value === "3") {
        return (<><i class="fa fa-exclamation fs-6 bg-primary p-1 current-icon"></i> <span>Returned</span></>)
    }

    if (value === "4") {
        return (<><i class="fa fa-exclamation fs-6 bg-danger p-1 current-icon"></i> <span>Canceled</span></>)
    }
}
