import React from 'react'
import { useState } from 'react'
import { toast } from 'react-toastify'
import Layout from '../../Components/Shop/Layout/Layout'
import Tostify from '../../Components/Tostify'
import { post } from '../../services'
import './style.css'

export default function ContactUs({ cartItems }) {

    const [name, setName] = useState('')
    const [subject, setSubject] = useState('')
    const [email, setEmail] = useState('')
    const [phone, setPhone] = useState('')
    const [message, setMessage] = useState('')

    const handleSubmit = (e) => {
        e.preventDefault();
        if (name !== '' && subject !== '' && email !== '' && phone !== '' && message !== '') {
            const payload = {
                name: name,
                subject: subject,
                email: email,
                phone: phone,
                message: message,
            }
            post('/contactUs/contactUsAPI.php',
                JSON.stringify(payload)).then(res => { })
                .then(function (response) {
                    setName('');
                    setSubject('');
                    setEmail('');
                    setPhone('');
                    setMessage('');
                    toast['success']('Your Message Inserted Successfully !!')
                })
            document.getElementById('name').style.borderColor = ''
            document.getElementById('subject').style.borderColor = ''
            document.getElementById('email').style.borderColor = ''
            document.getElementById('phone').style.borderColor = ''
            document.getElementById('message').style.borderColor = ''
        }
        else {
            toast['error']('Please fill all of the fields.')
            document.getElementById('name').style.borderColor = 'red'
            document.getElementById('subject').style.borderColor = 'red'
            document.getElementById('email').style.borderColor = 'red'
            document.getElementById('phone').style.borderColor = 'red'
            document.getElementById('message').style.borderColor = 'red'
        }
    }
    return (
        <>
            <Layout cartItems={cartItems} pgTitle='Contact Us'>
                <Tostify />
                <section id="contact-us" className="contact-us section">
                    <div className="container">
                        <div className="contact-head">
                            <div className="row">
                                <div className="col-12">
                                    <div className="section-title">
                                        <h2>Contact Us</h2>
                                    </div>
                                </div>
                            </div>
                            <div className="contact-info">
                                <div className="row">
                                    <div className="col-lg-4 col-md-12 col-12">
                                        <div className="single-info-head">
                                            <div className="single-info">
                                                <i className="lni lni-map"></i>
                                                <h3>Address</h3>
                                                <ul>
                                                    <li>44 Shirley Ave. Mashhad,<br /> IL 60185, Iran.</li>
                                                </ul>
                                            </div>
                                            <div className="single-info">
                                                <i className="lni lni-phone"></i>
                                                <h3>Call us on</h3>
                                                <ul>
                                                    <li><a href="tel:+18005554400">+98 915 504 26 78 (Toll free)</a></li>
                                                    <li><a href="tel:+321556667890">+98 905 521 21 35</a></li>
                                                </ul>
                                            </div>
                                            <div className="single-info">
                                                <i className="lni lni-envelope"></i>
                                                <h3>Mail at</h3>
                                                <ul>
                                                    <li><a href="mailto:support@shopgrids.com">support@digiware.com</a>
                                                    </li>
                                                    <li><a href="mailto:career@shopgrids.com">kiumars.dev@gmail.com</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-8 col-md-12 col-12">
                                        <div className="contact-form-head">
                                            <div className="form-main">
                                                <form className="form" onSubmit={handleSubmit}>
                                                    <div className="row">
                                                        <div className="col-lg-6 col-md-6 col-12">
                                                            <div className="form-group">
                                                                <input name="name" type="text" id='name' placeholder="Your Name"
                                                                    value={name} onChange={(e) => setName(e.target.value)} />
                                                            </div>
                                                        </div>
                                                        <div className="col-lg-6 col-md-6 col-12">
                                                            <div className="form-group">
                                                                <input name="subject" type="text" id='subject' placeholder="Your Subject"
                                                                    value={subject} onChange={(e) => setSubject(e.target.value)} />
                                                            </div>
                                                        </div>
                                                        <div className="col-lg-6 col-md-6 col-12">
                                                            <div className="form-group">
                                                                <input name="email" type="email" id='email' placeholder="Your Email"
                                                                    value={email} onChange={(e) => setEmail(e.target.value)} />
                                                            </div>
                                                        </div>
                                                        <div className="col-lg-6 col-md-6 col-12">
                                                            <div className="form-group">
                                                                <input name="phone" type="text" id='phone' placeholder="Your Phone"
                                                                    value={phone} onChange={(e) => setPhone(e.target.value)} />
                                                            </div>
                                                        </div>
                                                        <div className="col-12">
                                                            <div className="form-group message">
                                                                <textarea name="message" placeholder="Your Message" id='message' value={message} onChange={(e) => setMessage(e.target.value)}></textarea>
                                                            </div>
                                                        </div>
                                                        <div className="col-12">
                                                            <div className="form-group button">
                                                                <button type="submit" className="btn">Submit Message</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </Layout>
        </>
    )
}
