export function useGetDate() {
    const today = new Date();
    const year = today.getFullYear();
    const mes = today.getMonth() + 1;
    const dia = today.getDate();
    const date = dia + "/" + mes + "/" + year;
    return date
}
