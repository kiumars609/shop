import { useState, useEffect } from 'react'
import { get } from '../services';
// First go to website : https://geolocation-db.com/
// and login/register to website.
// Options{IPv4 , city , country_code , country_name , latitude , longitude , postal , state}
export function GetUserIP(request) {
    const [data, setData] = useState()
    useEffect(() => {
        get('https://geolocation-db.com/json/50ad4a90-fd5e-11ec-b463-1717be8c9ff1')
            .then(response => setData(response[request]))
    }, [request]);
    return data;
}