import { useEffect, useState } from 'react'
import { get } from '../services';

export function GetAPI({ url, defaltValue }) {
    const [data, setData] = useState(defaltValue);
    useEffect(() => {
        get(url)
            .then(response => {
                setData(response)
            })
    }, [url]);
    return data;
}