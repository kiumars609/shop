import { useEffect, useState } from 'react'

export default function CartItems() {
    const cartItemsFormLocalStorage = JSON.parse(localStorage.getItem("cartItems")) || []
    const [cartItems, setCartItems] = useState(cartItemsFormLocalStorage)

    useEffect(() => {
        localStorage.setItem("cartItems", JSON.stringify(cartItems));
    }, [cartItems]);

    const handleAddProduct = (product) => {
        const ProductExist = cartItems.find((item) => item.id === product.id && item.category === product.category);

        if (ProductExist) {
            setCartItems(cartItems.map((item) => item.id === product.id & item.category === product.category ?
                { ...ProductExist, quantity: ProductExist.quantity + 1 } : item));
        }
        else {
            setCartItems([...cartItems, { ...product, quantity: 1 }])
        }
    }

    const handleRemoveProduct = (product) => {
        const ProductExist = cartItems.find((item) => item.id === product.id);
        if (ProductExist.quantity === 1) {
            setCartItems(cartItems.filter((item) => item.id !== product.id));
            localStorage.setItem("cartItems", cartItems)
        }
        else {
            setCartItems(
                cartItems.map((item) => item.id === product.id ? { ...ProductExist, quantity: ProductExist.quantity - 1 } : item)
            )
            localStorage.setItem("cartItems", cartItems)
        }
    }

    const handleCartClearance = () => {
        setCartItems([]);
    }

    return { cartItems, handleAddProduct, handleRemoveProduct, handleCartClearance }
}