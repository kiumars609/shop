export function ProductsApi() {
    // Products
    const laptop = '/homepage/trendingPosts/laptop/laptopAPI.php';
    const mobile = '/homepage/trendingPosts/mobile/mobileAPI.php';
    const console = '/homepage/trendingPosts/console/consoleAPI.php';

    // HomePage
    const HPLaptop = '/homepage/trendingPosts/laptop/laptopAPI.php';
    const HPMobile = '/homepage/trendingPosts/mobile/mobileAPI.php';
    const HPConsole = '/homepage/trendingPosts/console/consoleAPI.php';
    return { HPLaptop, HPMobile, HPConsole, laptop, mobile, console }
}


